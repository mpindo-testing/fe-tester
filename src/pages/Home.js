import Navbar from './../components/AppBar'
import Carousel from './../components/Carousel';
import Footer from './../components/Footer';
import Main from './../components/Main';
import { useSelector } from 'react-redux'

function Home() {

  const auth = useSelector(state => state.auth)

  return (
    <div>
      <Navbar/>
      { !auth.login ? 
        <Carousel/> 
        : null
      }
      <Main/>
      <Footer/>
    </div> 
  );
}

export default Home;
