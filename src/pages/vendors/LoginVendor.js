import Navbar from '../../components/AppBar'
import Footer from '../../components/Footer';
import SignIn from '../../components/SignIn';
import NavbarVendor from '../../components/vendor/AppBarVendor';
import SignInVendor from '../../components/vendor/SignInVendor';

function LoginVendor() {
  return (
    <div>
      <NavbarVendor/>
      <SignInVendor/>
      <Footer/>
    </div> 
  );
}

export default LoginVendor;
