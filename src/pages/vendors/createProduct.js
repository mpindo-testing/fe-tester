import React, { useState, useEffect } from 'react'
import Navbar from '../../components/AppBar'
import Footer from '../../components/Footer';
import { Container } from '@material-ui/core'
import Spreadsheet from "react-spreadsheet";

import {
    DataSheetGrid,
    checkboxColumn,
    textColumn,
    keyColumn,
  } from 'react-datasheet-grid'

function CreateProduct() {
    const [ data, setData ] = useState([
        { sku: "", product_name: '', description: '', price: '', stock: ''  }
      ])
    
      const columns = [
        { ...keyColumn('sku', textColumn), title: 'SKU' },
        { ...keyColumn('product_name', textColumn), title: 'PRODUCT NAME' },
        { ...keyColumn('description', textColumn), title: 'DESCRIPTION' },
        { ...keyColumn('price', textColumn), title: 'PRICE' },
        { ...keyColumn('stock', textColumn), title: 'STOCK' },
      ]
      console.log(data,'data')

    return (
        <div>
        <Navbar/>
        <Container style={{ minHeight: 750 }}>
            <h1>Create product with copy paste</h1>

            <div style={{ paddingTop: 20 }}>
                <DataSheetGrid
                data={data}
                onChange={setData}
                columns={columns}
                />
            </div>
        </Container>
        <Footer/>
        </div>
    )
}

export default CreateProduct
