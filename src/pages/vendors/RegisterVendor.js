import Footer from '../../components/Footer';
import NavbarVendor from '../../components/vendor/AppBarVendor';
import SignUpVendor from '../../components/vendor/SignUpVendor';

function RegisterVendor() {
  return (
    <div>
      <NavbarVendor/>
      <SignUpVendor/>
      <Footer/>
    </div> 
  );
}

export default RegisterVendor;
