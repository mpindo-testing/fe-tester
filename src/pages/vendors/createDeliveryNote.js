import React, { useState, useEffect } from 'react'
import NavbarVendor from '../../components/vendor/AppBarVendor';
import Footer from '../../components/Footer';
import { Container, Box, Breadcrumbs, Typography, Link, Button, Paper } from '@material-ui/core';
import TableItemDelivery from '../../components/vendor/TableItemDelivery';
import uuid from 'react-uuid'

function CreateDeliveryNote() {

    const [dataPackage, setDataPackage] = useState([]);
    const [loaded, setLoaded] = useState(false);

    // console.log(dataPackage, 'dataPackage')

    // const idpackage = uuid()

    const onClickUpdateData = (value) => {
        setDataPackage(value)
    }

    const idpackage = uuid()

    const items = [
        {
            id: '123fdf46143',
            name: 'Fraser Gallop Cabernet Merlot 2019',
            category: 'Alcohol',
            id_package: idpackage,
            brand: 'Blanc',
            note: '',
            qty: 34,
            price: 4565100,
        },
        {
            id: '12346asdf1d43',
            name: 'Fraser Gallop Merlot 2017',
            id_package: idpackage,
            category: 'Alcohol',
            brand: 'Blanc',
            note: '',
            qty: 34,
            price: 4565100,
        },
        {
            id: '12ad346asdf143',
            name: 'Paysan Breton - Butter Unsalted (200G)',
            category: 'Meat',
            id_package: idpackage,
            brand: 'Paysan ',
            note: '',
            qty: 34,
            price: 4565100,
          },
          {
            id: '123df46asdf143',
            name: 'Europomella - Mozzarella Cheese in Brine Frozen (100g) Frozen',
            category: 'Meat',
            brand: 'Paysan ',
            note: '',
            id_package: idpackage,
            qty: 34,
            price: 4565100,
          },
    ]

    useEffect(() => {
        setTimeout(() => {
            setDataPackage(items)
            setLoaded(true)
        }, 100);
    }, []);
    

    return (
        <div>
        <NavbarVendor/>

        <Container style={{ minHeight: 750 }}>
            <Box pt={2} pb={2} pl={2}>
                <Breadcrumbs aria-label="breadcrumb">
                    <Link color="inherit" href="/dashboard" >
                        Dashboard
                    </Link>
                    <Typography color="textPrimary">Delivery Note</Typography>
                </Breadcrumbs>
            </Box>
            <Box pt={2} pb={2} pl={2}>
                <h4>Create Delivery Note</h4>

                <Box pt={2}> 
                    <h6>Order ID : 1231248</h6>
                    <h6>Company : PT Luxofood Indonesia</h6>
                </Box>
            </Box>

            <Box pt={2} pb={2} pl={2}>
                <TableItemDelivery 
                    data={dataPackage} 
                    loaded={loaded} 
                    onClickUpdateData={onClickUpdateData}
                />
            </Box>


        </Container>
        <Footer/>
        </div>
    )
}

export default CreateDeliveryNote
