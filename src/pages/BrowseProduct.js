import Navbar from '../components/AppBar'
import Footer from '../components/Footer';
import Browse from '../components/Browse';

function BrowseProduct() {

  return (
    <div>
      <Navbar/>
      <Browse/>
      <Footer/>
    </div> 
  );
}

export default BrowseProduct;
