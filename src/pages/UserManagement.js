import React, { useEffect, useState }  from 'react'
import Navbar from '../components/AppBar'
import Footer from '../components/Footer';
import { Container, Box, Breadcrumbs, Typography, Link, Grid, TextField, Button, CircularProgress, MenuItem, Select } from '@material-ui/core';
import swal from 'sweetalert';
import UserTableList from '../components/UserTableList';

function UserManagement() {

//   const auth = useSelector(state => state.auth)
const [loading, setLoading] = useState(false);

const user_list = localStorage.getItem('users_list') ? JSON.parse(localStorage.getItem('users_list')) : null;

const [valueUser, setValueUser] = useState({
    name: "",
    email: "", 
    password: "", 
    role: "admin", 
    date: new Date()
});

const [dataUsers, setdataUsers] = useState([]);
const [loaded, setLoaded] = useState(false);


const onChangeValue = (e) => {
  setValueUser({...valueUser, 
      [e.target.name] : e.target.value
  })
}

const onClickSave = (e) => {
    e.preventDefault()

    setLoading(true)
    setTimeout(() => {
        let userss = [...dataUsers]
        userss.push(valueUser)
        localStorage.setItem('users_list', JSON.stringify(userss))
        swal("Success", "Success saved company detail", 'success')
        window.location.reload()
        setLoading(false)
    }, 2000);
  }

  useEffect(() => {
      if(user_list !== null) {
        setdataUsers(user_list)
        setLoaded(true)
      }
    // eslint-disable-next-line
  }, []);

  return (
    <div>
      <Navbar/>
        <Container style={{ minHeight: 850 }}>
            <Box pt={2} pb={2} pl={2}>
                <Breadcrumbs aria-label="breadcrumb">
                    <Link color="inherit" href="/" >
                        Home
                    </Link>
                    <Typography color="textPrimary">Users Management</Typography>
                </Breadcrumbs>
                </Box>
            <Box pb={2} pt={2} pl={2}>
                <Typography component="h2" variant="h5" style={{fontWeight: 600}}>
                    User List 
                </Typography>
            </Box> 

            <Box pt={2} pb={3}>
                <UserTableList dataUsers={dataUsers} loaded={loaded}/>
            </Box>

            <Box pb={2} pt={2} pl={2}>
                <Typography component="h2" variant="h5" style={{fontWeight: 600}}>
                   Create a User
                </Typography>
            </Box> 

            <Box pl={2} pt={4}>
            <form validate="true" onSubmit={onClickSave}>
                <Grid container spacing={3}>
                    <Grid item lg={2} md={2} sm={2} xs={6} >
                        <Typography >
                            Name
                        </Typography> 
                    </Grid>
                    <Grid item lg={10} md={10} sm={10} xs={6} >
                        <TextField
                            variant="filled"
                            required
                            fullWidth
                            size='small'
                            id="name"
                            label="Name"
                            value={valueUser.name}
                            name="name"
                            onChange={onChangeValue}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={3}>
                    <Grid item lg={2} md={2} sm={2} xs={6} >
                        <Typography >
                            Email Address
                        </Typography> 
                    </Grid>
                    <Grid item lg={10} md={10} sm={10} xs={6} >
                        <TextField
                            variant="filled"
                            required
                            fullWidth
                            size='small'
                            id="email"
                            label="Email Address"
                            value={valueUser.email}
                            name="email"
                            onChange={onChangeValue}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={3}>
                    <Grid item lg={2} md={2} sm={2} xs={6} >
                        <Typography >
                            Password
                        </Typography> 
                    </Grid>
                    <Grid item lg={10} md={10} sm={10} xs={6} >
                        <TextField
                            variant="filled"
                            required
                            fullWidth
                            type="password"
                            size='small'
                            id="password"
                            label="Password"
                            value={valueUser.password}
                            name="password"
                            onChange={onChangeValue}
                        />
                    </Grid>
                </Grid>
                
                <Grid container spacing={3}>
                    <Grid item lg={2} md={2} sm={2} xs={6} >
                        <Typography >
                           Role
                        </Typography> 
                    </Grid>
                    <Grid item lg={10} md={10} sm={10} xs={6} >
                        <Select
                            labelId="demo-simple-select-filled-label"
                            id="demo-simple-select-filled"
                            value={valueUser.role}
                            onChange={onChangeValue}
                            name="role"
                        >
                        <MenuItem value="admin">Admin</MenuItem>
                        <MenuItem value="requester">Purchase Requester</MenuItem>
                        <MenuItem value="approver">Purchase Approver</MenuItem>
                        <MenuItem value="accounting">Accounting</MenuItem>
                        </Select>
                    </Grid>
                </Grid>


                <Box style={{ display: 'flex', justifyContent: 'flex-end' }} pt={4} pb={4}>
                    <Button
                        type="submit"
                        variant="contained"
                        color="primary"
                        // className={classes.submit}
                    >
                        Create User 
                        {loading ? <CircularProgress size={20} color="inherit" style={{marginLeft: 20}} /> : null}
                    </Button>
                </Box>

            
            </form>

            </Box>
        </Container>
      <Footer/>
    </div> 
  );
}

export default UserManagement;
