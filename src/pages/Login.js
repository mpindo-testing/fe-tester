import Navbar from './../components/AppBar'
import Footer from './../components/Footer';
import SignIn from '../components/SignIn';

function Login() {
  return (
    <div>
      <Navbar/>
      <SignIn/>
      <Footer/>
    </div> 
  );
}

export default Login;
