import Navbar from '../components/AppBar'
import Footer from '../components/Footer';
import SignUp from '../components/SignUp';

function Register() {
  return (
    <div>
      <Navbar/>
      <SignUp/>
      <Footer/>
    </div> 
  );
}

export default Register;
