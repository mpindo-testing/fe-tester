import React, { useEffect, useState }  from 'react'
import Navbar from '../components/AppBar'
import Footer from '../components/Footer';
import { Container, Box, Breadcrumbs, Typography, Link } from '@material-ui/core';
import TableManageTemplate from '../components/TableManageTemplate';


function ManageTemplate() {

    const [dataPuchaseReq, setDataPuchaseReq] = useState([]);
    const [loaded, setLoaded] = useState(false);


    useEffect(() => {
        const getData = () => {
            let existingPO = localStorage.getItem("template_data") ? JSON.parse(localStorage.getItem("template_data")) : [] 
            setDataPuchaseReq(existingPO)
            setLoaded(true)
        }
        getData()
    }, []);

    return (
        <div>
            <Navbar/>
            <Container>
                <Box pt={2} pb={2} pl={2}>
                <Breadcrumbs aria-label="breadcrumb">
                    <Link color="inherit" href="/" >
                        Home
                    </Link>
                    <Typography color="textPrimary">Manage Template</Typography>
                </Breadcrumbs>
                </Box>
                <Box pt={2} pb={4}  style={{ minHeight: 750 }}>
                    <TableManageTemplate
                        loaded={loaded}
                        dataPuchaseReq={dataPuchaseReq}
                    />
                </Box>
            </Container>
            
            <Footer/>
        </div>
    )
}

export default ManageTemplate
