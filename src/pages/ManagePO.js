import React, { useEffect, useState }  from 'react'
import Navbar from './../components/AppBar'
import Footer from './../components/Footer';
import { Container, Box, Breadcrumbs, Typography, Link } from '@material-ui/core';
import TablePuchaseOrders from '../components/TablePuchaseOrders';

function ManagePO() {

    const [dataPuchaseReq, setDataPuchaseReq] = useState([]);
    const [loaded, setLoaded] = useState(false);


    useEffect(() => {
        const getData = () => {
            let existingPO = localStorage.getItem("purchase_request") ? JSON.parse(localStorage.getItem("purchase_request")) : [] 

            const arrayFilter = existingPO.filter(val => val.status === "Approved")

            setDataPuchaseReq(arrayFilter)
            setLoaded(true)
        }   
        getData()
    }, []);

    return (
        <div>
            <Navbar/>
            <Container>
                <Box pt={2} pb={2} pl={2}>
                <Breadcrumbs aria-label="breadcrumb">
                    <Link color="inherit" href="/" >
                    Home
                    </Link>
                    <Typography color="textPrimary">Manage Purchase Orders</Typography>
                </Breadcrumbs>
                </Box>
                <Box pt={2} pb={4} style={{ minHeight: 750 }}>
                    <TablePuchaseOrders
                        loaded={loaded}
                        dataPuchaseReq={dataPuchaseReq}
                    />
                </Box>
            </Container>
             
            <Footer/>
        </div>
    )
}

export default ManagePO
