import React, { useEffect, useState } from 'react'
import Navbar from './../components/AppBar'
import Footer from './../components/Footer';
import { Grid, Box, Container, 
  Link, Card, CardMedia, Paper, Button,  Dialog, DialogContent, Tooltip } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';
import { useSelector } from 'react-redux';
import useFetchProduct from '../Hook/useFetchProduct';
import { useParams } from 'react-router';
import { useHistory } from 'react-router-dom'

const HtmlTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: '#3f51b5',
    color: 'rgba(0, 0, 0, 0.87)',
    width: 'fit-content',
    fontSize: theme.typography.pxToRem(12),
    border: '2px solid #3f51b5',
  },
}))(Tooltip);

const useStyles = makeStyles((theme) =>  ({
  jumlah: {
      paddingLeft: 12,
      paddingRight: 12,
      fontSize:'19px',
      fontWeight: 600
  },

}))


function ProductPage() {

  const classes = useStyles();
  const location = useParams()
  const history = useHistory()

  const auth = useSelector(state => state.auth)

  const { products : productall, loaded : loadedproduct } = useFetchProduct()

  const [product, setProduct] = useState([]);
  const [loaded, setLoaded] = useState(false);
  const [vendor, setVendor] = useState("");
  const [vendorID, setVendorID] = useState();

  let existingPR = localStorage.getItem("purchase_request") ? JSON.parse(localStorage.getItem("purchase_request")) : []
  let existingTemplate = localStorage.getItem("template_data") ? JSON.parse(localStorage.getItem("template_data")) : []

  const [dataOpenPR, setdataOpenPR] = useState([]);
  const [dataOpenTemplate, setDataOpenTemplate] = useState([]);

  const [productsPick, setProductsPick] = useState();

  const [quantity, setQuantity] = useState(1);
  const [price, setPrice] = useState(""); 

  console.log(price, 'price')
  // const [fixedPrice, setFixedPrice] = useState("");

  const onClickAdd = () => {
    let value = quantity + 1
    let prices = value * product[0].price
    setQuantity(value)
    setPrice(prices)
}

const onClickRemove = () => {
    let value = quantity - 1
    let prices = value * product[0].price
    setQuantity(value)
    setPrice(prices)
}

  const [open, setOpen] = React.useState(false);
  const [openTemplate, setOpenTemplate] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
    let product_detail = {
      sku: product[0].sku,
      name: product[0].name,
      sub_price : price === "" ? product[0].price : parseInt(price),
      price: parseInt(product[0].price),
      quantity: quantity,
      vendor_id : vendorID,
      vendor_name : vendor
    }
    setProductsPick(product_detail)
  };

  const handleClickOpenTemplate = () => {
    setOpenTemplate(true);
    let product_detail = {
      sku: product[0].sku,
      name: product[0].name,
      sub_price : price === "" ? product[0].price : parseInt(price),
      price: parseInt(product[0].price),
      quantity: quantity,
      vendor_id : vendorID,
      vendor_name : vendor
    }
    setProductsPick(product_detail)
  };

  const handleClose = () => {
    setOpen(false);
  };
  const handleCloseTemplate = () => {
    setOpenTemplate(false);
  };

  const onClickUpdate = (value) => {
    history.push({
      pathname: `/update-purchase/${value.id}`,
      state: {  
        product: productsPick
      }
    })
  }

  const onClickUpdateToTemplate = (value) => {
    history.push({ 
      pathname: `/update-template/${value.id}`,
      state: {  
        product: productsPick
      }
    })
  }

  const onClickCreateNew = () => {
    history.push({
      pathname: `/create-purchases`,
      state: {  
        product: productsPick
      }
    })
  }

  const onClickCreateNewtemplate = () => {
    history.push({
      pathname: `/create-template`,
      state: {  
        product: productsPick
      }
    })
  }
  
  useEffect(() => {

    const findOne = () => {
      let data = [...productall]
      let one = data.filter(val => val.sku === location.id)
      let meta = one[0].meta_data

      for(let k= 0; k < meta.length; k ++) {
        if(meta[k].key === "woo_dropshipper") {
            setVendor(meta[k].value)
            setVendorID(meta[k].id.$numberInt)
          }
      }
      setProduct(one)
      setLoaded(true)
    }

    if(loadedproduct) {
      findOne()
    }
    // eslint-disable-next-line
  }, [loadedproduct]);

  useEffect(() => {
    if(existingPR.length !== 0) {
      let data= [...existingPR]
      let filterOpen = data.filter(val => val.status === "Open")
      setdataOpenPR(filterOpen)
    }
    // eslint-disable-next-line
  }, []);


  useEffect(() => {
    if(existingTemplate.length !== 0) {
      let data= [...existingTemplate]
      setDataOpenTemplate(data)
    }
    // eslint-disable-next-line
  }, []);

  return ( 
    <div>
        <Navbar/>
        <Container>
            <Box pt={2} pb={2}>
              <Breadcrumbs aria-label="breadcrumb">
                <Link color="inherit" href="/" >
                  Home
                </Link>
                <Typography color="textPrimary">{loaded && product[0].name}</Typography>
              </Breadcrumbs>
            </Box>
            <Box pt={2} pb={4} style={{ minHeight: 750 }}>
              <Grid container spacing={3}>
                <Grid item lg={4}>
                  <Card >
                    <div style={{display: 'flex', justifyContent: 'center'}}>
                      <CardMedia
                        style={{
                          height: 300,
                          width: 300,
                        }}
                        image={loaded && product[0].images[0].src}
                        title="product"
                      />
                    </div>
                  </Card>
                </Grid>
                <Grid item lg={8}>
                  <Paper>
                   <Box p={3}>
                      <Box>
                        <Typography variant="h5" component="h2">
                          {loaded && product[0].name}
                        </Typography>
                      </Box>
                      <Box pt={1}>
                        <Typography variant="body1" >
                          Vendor : {vendor}
                        </Typography>
                      </Box>
                      {/* <Box pt={1}>
                        <Typography variant="body2" >
                          Stock : 28
                        </Typography>
                      </Box> */}
                      <Box pt={2} pb={2}>
                        <Typography variant="h5" fontWeight="bold" >
                          { auth.login ? `Rp. ${loaded && product[0].price.toLocaleString()}` : "Rp. Login to View Price" }
                        </Typography>
                      </Box>

                      {auth.login ? 
                      <Box pt={3} pb={4}>
                        <Grid container  >
                          <Grid item>
                            <Fab color="default" aria-label="remove" size="small"
                              onClick={() => onClickRemove(quantity)}
                              disabled={quantity <= 1 ? true : false}
                            >
                                <RemoveIcon />
                            </Fab>
                            <span className={classes.jumlah}>{quantity}</span>
                            <Fab color="default" aria-label="add" size="small"
                              onClick={() => onClickAdd(quantity)}
                            >
                                <AddIcon />
                            </Fab>
                          </Grid>
                          <Grid item>
                            <Box pl={2} style={{paddingTop: 5}}>
                              <Button 
                                variant='contained' color='primary'
                                onClick={() => handleClickOpen()}
                                disabled={localStorage.getItem('role') === "approver" || localStorage.getItem('role') === "accounting"  ? true : false}
                                >ADD TO PURCHASE REQUEST</Button>
                            </Box>
                          </Grid>
                          <Grid item>
                            <Box pl={2} style={{paddingTop: 5}}>
                              <Button 
                                variant='contained' color='default'
                                onClick={() => handleClickOpenTemplate()}
                                disabled={localStorage.getItem('role') === "approver" || localStorage.getItem('role') === "accounting"  ? true : false}
                                >ADD TO TEMPLATE</Button>
                            </Box>
                          </Grid>

                        </Grid>
                      </Box> : null }

                      <Box pt={1} pb={2}>
                        <Typography variant="body2" >
                          Delivery Tomorrow, last order 23:59
                        </Typography>
                      </Box>

                      <Box pt={1}>
                        <Typography variant="body1" >
                        Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.
                        </Typography>
                      </Box>

                   </Box>
                  </Paper>
                </Grid>
              </Grid>
            </Box>
            <Dialog 
              onClose={handleClose} 
              aria-labelledby="simple-dialog-title"  
              open={open}
              size="lg"
            >
                <DialogContent>
                  <Box pt={2}>
                  <div onClick={() => onClickCreateNew()}>
                  <Button variant="contained" color="secondary" size="medium">
                      Create new purchase request
                  </Button>
                  </div>
                  </Box>
                  <Box pt={2} pb={2} style={{display: 'flex', justifyContent: 'center'}}>OR</Box>
                  { dataOpenPR.length === 0 ? 
                  <Box pb={4} style={{display: 'flex', justifyContent: 'center'}}>There is no Purchase Request Open</Box> :
                  
                  <ul className="list-pr">
                    { dataOpenPR && dataOpenPR.map((val, i) => (
                    <li key={i}>
                      <HtmlTooltip
                          title={
                            <React.Fragment>
                              <div style={{padding: 10}}>
                                <ul style={{ listStyle: 'none', paddingTop: 10, paddingRight: 5, paddingLeft: 5, color: '#fff' }}>
                                  { val.data.map((name, i) => (
                                    <li key={i}>{i + 1}. {" "} {name.name}</li>
                                  )) }
                                </ul>
                              </div>
                            </React.Fragment>
                          }
                          placement="right"
                          arrow
                        >
                          <div className="buttonhover" onClick={() => onClickUpdate(val)}>
                            Add to PO ID #{val.id}
                          </div>
                      </HtmlTooltip>
                    </li>
                    )) }
                  </ul>
                  }
                  
                </DialogContent>
            </Dialog>


            <Dialog 
              onClose={handleCloseTemplate} 
              aria-labelledby="simple-dialog-title"  
              open={openTemplate}
              size="lg"
            >
              <DialogContent style={{ minWidth: 500}}>
                { dataOpenTemplate.length === 0 ? 
                <Box pb={4} pt={4} style={{display: 'flex', justifyContent: 'center', flexDirection: 'column', alignItems: 'center'}}>
                  <Typography>You dont have any template yet.</Typography> <br/>
                  <Box onClick={() => onClickCreateNewtemplate()}>
                    <Button variant="contained" color="secondary" size="medium">
                        Create New Template
                    </Button>
                  </Box>
                </Box> :
                <>
                <Box pt={2} style={{width: '100%'}}>
                  <Box onClick={() => onClickCreateNewtemplate()}>
                    <Button fullWidth variant="contained" color="secondary" size="medium">
                        Create New Template
                    </Button>
                  </Box>
                </Box>
                <Box pt={2} style={{display: 'flex', justifyContent: 'center'}}>OR</Box>
                <ul className="list-pr">
                  { dataOpenTemplate && dataOpenTemplate.map((val, i) => (
                  <li key={i}>
                    <HtmlTooltip
                        title={
                          <React.Fragment>
                            <div style={{padding: 10}}>
                              <ul style={{ listStyle: 'none', paddingTop: 10, paddingRight: 5, paddingLeft: 5, color: '#fff' }}>
                                { val.data.map((name, i) => (
                                  <li key={i}>{i + 1}. {" "} {name.name}</li>
                                )) }
                              </ul>
                            </div>
                          </React.Fragment>
                        }
                        placement="right"
                        arrow
                      >
                        <div className="buttonhover" onClick={() => onClickUpdateToTemplate(val)}>
                          Add to {val.name}
                        </div>
                    </HtmlTooltip>
                  </li>
                  )) }
                </ul>
                </>}
                
              </DialogContent>
            </Dialog>
        </Container>
        <Footer/>
    </div> 
  );
}

export default ProductPage;
