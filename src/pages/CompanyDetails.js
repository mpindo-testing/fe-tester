import React, { useEffect, useState }  from 'react'
import Navbar from '../components/AppBar'
import Footer from '../components/Footer';
import { Container, Box, Breadcrumbs, Typography, Link, Grid, TextField, Button, CircularProgress } from '@material-ui/core';
import swal from 'sweetalert';

function CompanyDetail() {

//   const auth = useSelector(state => state.auth)
const [loading, setLoading] = useState(false);

const company_detail = localStorage.getItem('company_detail') ? JSON.parse(localStorage.getItem('company_detail')) : null;

const [valueCompany, setValueCompany] = useState({
    legal_name: "",
    address: "", 
    phone: "", 
    email: localStorage.getItem('user_registered') ? JSON.parse(localStorage.getItem('user_registered')).email : "", 
    website: "", 
    nib_number: "", 
    npwp_number: "", 
    ig_username: "", 
    fb_username: "", 
});


const onChangeValue = (e) => {
  setValueCompany({...valueCompany, 
      [e.target.name] : e.target.value
  })
}

const onClickSave = (e) => {
    e.preventDefault()

    setLoading(true)
    setTimeout(() => {
        localStorage.setItem('company_detail', JSON.stringify(valueCompany))
        localStorage.setItem('first_login', false)
        swal("Success", "Success saved company detail", 'success')
        setLoading(false)
        setTimeout(() => {
            window.location.href = '/user-management'
        }, 1000);
    }, 2000);
  }


  useEffect(() => {
    if(company_detail !== null) {
        setValueCompany({...valueCompany, 
            legal_name: company_detail.legal_name,
            address: company_detail.address, 
            phone: company_detail.phone, 
            email: company_detail.email, 
            website: company_detail.website, 
            nib_number: company_detail.nib_number, 
            npwp_number: company_detail.npwp_number, 
            ig_username: company_detail.ig_username, 
            fb_username: company_detail.fb_username, 
        })
    }
    // eslint-disable-next-line
  }, []);

  return (
    <div>
      <Navbar/>
        <Container style={{ minHeight: 850 }}>
            <Box pt={2} pb={2} pl={2}>
                <Breadcrumbs aria-label="breadcrumb">
                    <Link color="inherit" href="/" >
                        Home
                    </Link>
                    <Typography color="textPrimary">Company Detail</Typography>
                </Breadcrumbs>
                </Box>
            <Box pb={2} pt={2} pl={2}>
                <Typography component="h2" variant="h5" style={{fontWeight: 600}}>
                    Please Provide the Company Detail Information
                </Typography>
            </Box> 
            <Box pl={2} pt={4}>
            <form validate="true" onSubmit={onClickSave}>
                <Grid container spacing={3}>
                    <Grid item lg={2} md={2} sm={2} xs={6} >
                        <Typography >
                            Legal Name
                        </Typography> 
                    </Grid>
                    <Grid item lg={10} md={10} sm={10} xs={6} >
                        <TextField
                            variant="filled"
                            required
                            fullWidth
                            size='small'
                            id="legal_name"
                            label="Legal Name"
                            value={valueCompany.legal_name}
                            name="legal_name"
                            onChange={onChangeValue}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={3}>
                    <Grid item lg={2} md={2} sm={2} xs={6} >
                        <Typography >
                            Address
                        </Typography> 
                    </Grid>
                    <Grid item lg={10} md={10} sm={10} xs={6} >
                        <TextField
                            variant="filled"
                            required
                            fullWidth
                            size='small'
                            id="address"
                            label="Address"
                            value={valueCompany.address}
                            name="address"
                            onChange={onChangeValue}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={3}>
                    <Grid item lg={2} md={2} sm={2} xs={6} >
                        <Typography >
                            Phone
                        </Typography> 
                    </Grid>
                    <Grid item lg={10} md={10} sm={10} xs={6} >
                        <TextField
                            variant="filled"
                            required
                            fullWidth
                            size='small'
                            id="phone"
                            label="Phone"
                            value={valueCompany.phone}
                            name="phone"
                            onChange={onChangeValue}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={3}>
                    <Grid item lg={2} md={2} sm={2} xs={6} >
                        <Typography >
                            Email Address
                        </Typography> 
                    </Grid>
                    <Grid item lg={10} md={10} sm={10} xs={6} >
                        <TextField
                            variant="filled"
                            required
                            fullWidth
                            size='small'
                            id="email"
                            label="Email Address"
                            value={valueCompany.email}
                            name="email"
                            onChange={onChangeValue}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={3}>
                    <Grid item lg={2} md={2} sm={2} xs={6} >
                        <Typography >
                            Website
                        </Typography> 
                    </Grid>
                    <Grid item lg={10} md={10} sm={10} xs={6} >
                        <TextField
                            variant="filled"
                            required
                            fullWidth
                            size='small'
                            id="website"
                            label="Website"
                            value={valueCompany.website}
                            name="website"
                            onChange={onChangeValue}
                        />
                    </Grid>
                </Grid>
                
                <Grid container spacing={3}>
                    <Grid item lg={2} md={2} sm={2} xs={6} >
                        <Typography >
                            NIB number
                        </Typography> 
                    </Grid>
                    <Grid item lg={10} md={10} sm={10} xs={6} >
                        <TextField
                            variant="filled"
                            required
                            fullWidth
                            size='small'
                            id="nib_number"
                            label="NIB number"
                            value={valueCompany.nib_number}
                            name="nib_number"
                            onChange={onChangeValue}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={3}>
                    <Grid item lg={2} md={2} sm={2} xs={6} >
                        <Typography >
                            NPWP number
                        </Typography> 
                    </Grid>
                    <Grid item lg={10} md={10} sm={10} xs={6} >
                        <TextField
                            variant="filled"
                            required
                            fullWidth
                            size='small'
                            id="npwp_number"
                            label="NPWP number"
                            value={valueCompany.npwp_number}
                            name="npwp_number"
                            onChange={onChangeValue}
                        />
                    </Grid>
                </Grid>

                <Grid container spacing={3}>
                    <Grid item lg={2} md={2} sm={2} xs={6} >
                        <Typography >
                            Instagram Username
                        </Typography> 
                    </Grid>
                    <Grid item lg={10} md={10} sm={10} xs={6} >
                        <TextField
                            variant="filled"
                            required
                            fullWidth
                            size='small'
                            id="ig_username"
                            label="Instagram Username"
                            value={valueCompany.ig_username}
                            name="ig_username"
                            onChange={onChangeValue}
                        />
                    </Grid>
                </Grid>

                <Grid container spacing={3}>
                    <Grid item lg={2} md={2} sm={2} xs={6} >
                        <Typography >
                            Facebook Username
                        </Typography> 
                    </Grid>
                    <Grid item lg={10} md={10} sm={10} xs={6} >
                        <TextField
                            variant="filled"
                            required
                            fullWidth
                            size='small'
                            id="fb_username"
                            label="Facebook Username"
                            value={valueCompany.fb_username}
                            name="fb_username"
                            onChange={onChangeValue}
                        />
                    </Grid>
                </Grid>


                <Box style={{ display: 'flex', justifyContent: 'flex-end' }} pt={4} pb={4}>
                    <Button
                        type="submit"
                        variant="contained"
                        color="primary"
                        // className={classes.submit}
                    >
                        Save Data 
                        {loading ? <CircularProgress size={20} color="inherit" style={{marginLeft: 20}} /> : null}
                    </Button>
                </Box>

            
            </form>

            </Box>
        </Container>
      <Footer/>
    </div> 
  );
}

export default CompanyDetail;
