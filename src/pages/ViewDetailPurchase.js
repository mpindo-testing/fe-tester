import { useState, useEffect } from 'react'
import Navbar from '../components/AppBar'
import Footer from '../components/Footer';
import { Container, Box, Breadcrumbs, Typography, Link,  Button, TextField } from '@material-ui/core';
import TableCartUpdatePR from '../components/TableCartUpdatePR';
import { useParams } from 'react-router-dom'
import { find, remove } from 'lodash'
import swal from 'sweetalert';

function ViewDetailPurchase() {

    const params = useParams()

    const [dataCart, setDataCart] = useState([]);
    const [dataCartAll, setDataCartAll] = useState({});
    const [noteValue, setNoteValue] = useState("");

    const approvePurchaseRequest = () => {
        
        let existingPO = localStorage.getItem("purchase_request") ? JSON.parse(localStorage.getItem("purchase_request")) : []
        let recentProducts = localStorage.getItem("recent_product") ? JSON.parse(localStorage.getItem("recent_product")) : null

        let array = [...existingPO]
        const unremove_item = remove(array, function(n) {
        return n.id !== dataCartAll.id
        });

        let newArray = [...unremove_item]
        
        let finalPO = {
            id: dataCartAll.id,
            data : dataCartAll.data,
            total_price : dataCartAll.total_price,
            author : dataCartAll.author,
            status: 'Approved', 
            note : noteValue,
            date : new Date()
        }

        newArray.push(finalPO)
        localStorage.setItem('purchase_request', JSON.stringify(newArray))

        if(recentProducts !== null) {
            let newArrayPord = [...recentProducts]
            const newRecent = newArrayPord.concat(dataCartAll.data);
            localStorage.setItem("recent_product", JSON.stringify(newRecent))
        } else {
            localStorage.setItem("recent_product", JSON.stringify(dataCartAll.data))
        }
        swal("Success", "Success Approve Purchase Request", 'success' )
        setTimeout(() => {
            window.location.href = "/manage-orders"
        }, 1000);
        
    }


    const rejectPurchaseRequest = () => {
        if(noteValue !== "") {
            let existingPO = localStorage.getItem("purchase_request") ? JSON.parse(localStorage.getItem("purchase_request")) : []

            let array = [...existingPO]
            const unremove_item = remove(array, function(n) {
            return n.id !== dataCartAll.id
            });

            let newArray = [...unremove_item]
            let finalPO = {
                id: dataCartAll.id,
                data : dataCartAll.data,
                total_price : dataCartAll.total_price,
                author : dataCartAll.author,
                status: 'Open', 
                note : noteValue,
                date : new Date()
            }

            newArray.push(finalPO)
            localStorage.setItem('purchase_request', JSON.stringify(newArray))
            swal("Success", "Success Rejected Purchase Request", 'success' )
            setTimeout(() => {
                window.location.href = "/manage-purchases"
            }, 1000);
        } else {
            swal("Error", "Note cannot be empty", 'error' )
        }
    }

    const cancelPurchaseRequest = () => {
        if(noteValue !== "") {
            let existingPO = localStorage.getItem("purchase_request") ? JSON.parse(localStorage.getItem("purchase_request")) : []

            let array = [...existingPO]
            const unremove_item = remove(array, function(n) {
            return n.id !== dataCartAll.id
            });

            let newArray = [...unremove_item]
            let finalPO = {
                id: dataCartAll.id,
                data : dataCartAll.data,
                total_price : dataCartAll.total_price,
                author : dataCartAll.author,
                status: 'Canceled', 
                note : noteValue,
                date : new Date()
            }

            newArray.push(finalPO)
            localStorage.setItem('purchase_request', JSON.stringify(newArray))
            swal("Success", "Success Canceled Purchase Request", 'success' )
            setTimeout(() => {
                window.location.href = "/manage-purchases"
            }, 1000);
        } else {
            swal("Error", "Note cannot be empty", 'error' )
        }
    }

    useEffect(() => {
        function proceedCart () {
            let existingPO = localStorage.getItem("purchase_request") ? JSON.parse(localStorage.getItem("purchase_request")) : [] 

            const currentCart = find(existingPO, ['id', params.id]);
            
            setDataCart(currentCart.data)
            setDataCartAll(currentCart)
        }
        proceedCart()
        // eslint-disable-next-line
    }, []);

    return (
        <div>
            <Navbar/>
            <Container>
                <Box pt={2} pb={2} pl={2}>
                <Breadcrumbs aria-label="breadcrumb">
                    <Link color="inherit" href="/" >
                        Home
                    </Link>
                    <Link color="inherit" href="/manage-purchases" >
                        View Purchase Request
                    </Link>
                    <Typography color="textPrimary">Detail </Typography>
                </Breadcrumbs>
                </Box>
                <Box pt={2} pb={4} pl={2} pr={2} style={{ minHeight: 750 }}>
                    <Box pt={2} pb={1}>
                        <Typography color="textPrimary" variant="h5" style={{ fontWeight: 700 }}>Detail Purchase Request</Typography>
                    </Box>
                    <Box pt={1} style={{ display: 'flex', justifyContent:'space-between' }}>
                        <Box>
                            <Typography color="textPrimary" variant="h6">PO ID : #{dataCartAll.id}</Typography>
                            <Typography color="textPrimary" variant="h6">Author : {dataCartAll.author}</Typography>
                        </Box>
                        <Box >
                            <Box style={{display: 'flex', justifyContent: 'space-between'}}>
                                <Box pr={2}><Button variant="contained" color="primary" size="medium" onClick={() => approvePurchaseRequest()}>Approve</Button></Box>
                                <Box pr={2}><Button variant="contained" color="default" size="medium" onClick={() => rejectPurchaseRequest()} >Reject</Button></Box>
                                <Box><Button variant="contained" color="secondary" size="medium" onClick={() => cancelPurchaseRequest()} >Cancel</Button></Box>
                            </Box>

                        </Box>
                    </Box>

                    <Box pt={2} >
                        <Box pb={1}>Note </Box>
                        <TextField 
                            type="textarea"
                            multiline
                            rows={4}
                            variant="outlined"
                            style={{ backgroundColor: 'white', width: '100%' }}
                            onChange={(e) => setNoteValue(e.target.value)}
                        />
                    </Box>

                    { dataCart.length !== 0 ? 
                    <Box pt={4} pb={2}>
                        <TableCartUpdatePR 
                            data={dataCart}
                            dataCartAll={dataCartAll}
                            setDataCart={setDataCart}
                            view={true}

                        />
                    </Box>
                    : null }
                    
                </Box>
            </Container>
            
            <Footer/>
        </div>
    )
}

export default ViewDetailPurchase;
