import NavbarVendor from '../components/vendor/AppBarVendor';
import Footer from './../components/Footer';
import { Box, Container } from '@material-ui/core'
import OrderTabs from '../components/vendor/OrderTabs';

function Dashboard() {
  return (
    <div>
      <NavbarVendor/>
        <Container style={{ minHeight: 900,}}>
          <Box pl={3} pr={3}>
              <Box pt={4}><h4>Order Lists </h4></Box>
              <Box pt={2}> 
                  <OrderTabs/>
              </Box>  
          </Box>
        </Container>
      <Footer/>
    </div> 
  );
}

export default Dashboard;
