import { useState, useEffect } from 'react'
import Navbar from '../components/AppBar'
import Footer from '../components/Footer';
import { Container, Box, Breadcrumbs, Typography, Link } from '@material-ui/core';
import TableCartUpdatePR from '../components/TableCartUpdatePR';
import { useParams } from 'react-router-dom'
import { find} from 'lodash';

function ViewDetailOrders() {

    const params = useParams()

    const [dataCart, setDataCart] = useState([]);
    const [dataCartAll, setDataCartAll] = useState({});

    useEffect(() => {
        function proceedCart () {
            let existingPO = localStorage.getItem("purchase_request") ? JSON.parse(localStorage.getItem("purchase_request")) : [] 

            const currentCart = find(existingPO, ['id', params.id]);
            
            setDataCart(currentCart.data)
            setDataCartAll(currentCart)
        }
        proceedCart()
          // eslint-disable-next-line
    }, []);

    return (
        <div>
            <Navbar/>
            <Container>
                <Box pt={2} pb={2} pl={2}>
                <Breadcrumbs aria-label="breadcrumb">
                    <Link color="inherit" href="/" >
                        Home
                    </Link>
                    <Link color="inherit" href="/manage-purchases" >
                        View Purchase { dataCartAll.status!== "Approved" ? "Request" : "Orders" }
                    </Link>
                    <Typography color="textPrimary">Detail </Typography>
                </Breadcrumbs>
                </Box>
                <Box pt={2} pb={4} pl={2} pr={2} style={{ minHeight: 750 }}>
                    <Box pt={2} pb={1}>
                        <Typography color="textPrimary" variant="h5" style={{ fontWeight: 700 }}>Detail Purchase { dataCartAll.status!== "Approved" ? "Request" : "Orders" }</Typography>
                    </Box>
                    <Box pt={1} style={{ display: 'flex', justifyContent:'space-between' }}>
                        <Box>
                            <Typography color="textPrimary" variant="h6">PO ID : #{dataCartAll.id}</Typography>
                            <Typography color="textPrimary" variant="h6">Author : {dataCartAll.author}</Typography>
                        </Box>
                        <Box >
                            <Typography color="textPrimary" variant="h6">Status : {dataCartAll.status}</Typography>
                        </Box>
                    </Box>


                    { dataCart.length !== 0 ? 
                    <Box pt={4} pb={2}>
                        <TableCartUpdatePR 
                            data={dataCart}
                            dataCartAll={dataCartAll}
                            setDataCart={setDataCart}
                            view={true}

                        />
                    </Box>
                    : null }
                    
                </Box>
            </Container>
            
            <Footer/>
        </div>
    )
}

export default ViewDetailOrders;
