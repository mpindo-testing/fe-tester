import { useState, useEffect } from 'react'
import Navbar from '../components/AppBar'
import Footer from '../components/Footer';
import { Container, Box, Breadcrumbs, Typography, Link, Grid, Button, Paper, TextField } from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton'; 
import Select from 'react-select'
import useFetchProduct from '../Hook/useFetchProduct';
import TableCartTemplate from '../components/TableCartTemplate';
import productsList from '../Hook/listProducts.json'
import { useLocation } from 'react-router-dom'

function CreateTemplate() {
    const location = useLocation()

    // console.log(location, 'location')
    
    const { products, loaded } = useFetchProduct()

    const [productList, setproductList] = useState([]);
    const [vendorList, setVendorList] = useState([]);
    const [productSKUList, setProductSKUList] = useState([]);
    const [dataCart, setDataCart] = useState([]);
    const [loadedSKU, setLoadedSKU] = useState(false);
    const [loadedProduct, setLoadedProduct] = useState(false);
    const [loadedVendor, setLoadedVendor] = useState(false);

    const [templateName, setTemplateName] = useState("");

    // console.log(dataCart, 'data cart parent')

    const [quantity, setQuantity] = useState(1);
    const [price, setPrice] = useState(""); 
    const [fixedPrice, setFixedPrice] = useState("");

    const [valueProduct, setValueProduct] = useState({
        data: []
    })
    const [valueSKU, setValueSKU] = useState({
        data: []
    })
    const [valueVendor, setValueVendor] = useState({
        data: []
    })

    const handleChangeProduct = value => {
        setValueProduct({...valueProduct, 
            data: value
        });    
        setValueSKU({...valueSKU, 
            data: {
                value : value.sku,
                label: value.sku,
                product: value.name,
                price: value.price
            }
        });
        setPrice(value.price)
        setFixedPrice(value.price)
    }; 

    const handleChangeSKU = value => {
        setValueSKU({...valueSKU, 
            data: value
        });    
        setValueProduct({...valueProduct, 
            data: {
                value : value.product,
                label: value.product,
                sku: value.sku,
                price: value.price
            }
        });
        setPrice(value.price)
        setFixedPrice(value.price)
         
    }; 

    const handleChangeVendor = value => {
        setValueVendor({...valueVendor, 
            data: value
        });   
    }; 


    const onClickAdd = () => {
        let value = quantity + 1
        let prices = value * fixedPrice
        setQuantity(value)
        setPrice(prices)
    }

    const onClickRemove = () => {
        let value = quantity - 1
        let prices = value * fixedPrice
        setQuantity(value)
        setPrice(prices)
    }

    const onClickReset = () => {
        setValueSKU({...valueSKU, 
            data: []
        });    
        setValueProduct({...valueProduct, 
            data: []
        });
        setValueVendor({...valueVendor, 
            data: []
        });  
        setPrice()
        setFixedPrice()
        setQuantity(1)
    }

    const onClickToCart = () => {
        let newData = [...dataCart]
        newData.push({
            sku: valueSKU.data.value,
            name: valueProduct.data.value,
            sub_price : parseInt(price),
            price: parseInt(fixedPrice),
            quantity: quantity,
            vendor_id : valueVendor.data.value,
            vendor_name : valueVendor.data.label
        })
        setDataCart(newData)
        onClickReset()
    }

    useEffect(() => {
        function proceedVendor () {
            let data = [...products]
            let vendor = []
            for(let i= 0; i < data.length; i++) {
                for(let k=0; k < data[i].meta_data.length ; k ++ ) {
                    if(data[i].meta_data[k].key === "woo_dropshipper") {
                        vendor.push({
                            label : data[i].meta_data[k].value,
                            value : data[i].meta_data[k].id.$numberInt
                        })
                    }
                }
            }
            const unique = [...new Map(vendor.map(item => [item['label'], item])).values()]
            const filterdata = unique.filter(value => value.label !== "--")

            setVendorList(filterdata)
            setLoadedVendor(true)

        }
        if(loaded) {
            proceedVendor()
        }
        localStorage.removeItem('temp_template')
        // eslint-disable-next-line
    }, [loaded]);

    useEffect(() => {
        function proceedCart () {
            let array = []
            array.push(location.state.product)
            setDataCart(array)

        }
        if(location.state) {
            proceedCart()
        }
        // eslint-disable-next-line
    }, []);


    useEffect(() => {
        function proceedProduct () {
            let copy = [...productsList]

            let vendor_name = valueVendor.data.label
            let newdata = []
            for(let h = 0; h < copy.length; h ++) {
                for(let k=0; k < copy[h].meta_data.length ; k ++ ) {
                    if(copy[h].meta_data[k].key === "woo_dropshipper") {
                        if(copy[h].meta_data[k].value === vendor_name) {
                            newdata.push(copy[h])
                        }
                    }
                }
            }

            let product = []
            for(let i = 0; i < newdata.length; i ++ ) {
                    product.push({
                        value : newdata[i].name,
                        label: newdata[i].name,
                        sku: newdata[i].sku,
                        price: newdata[i].price
                    })
            }

            let sku = []
            for(let k = 0; k < newdata.length; k ++ ) {
                    sku.push({
                        value : newdata[k].sku,
                        label: newdata[k].sku,
                        product: newdata[k].name,
                        price: newdata[k].price
                    })
            }

            let finalsku = sku.filter(a => a.value !== "" )
            setProductSKUList(finalsku)
            setLoadedSKU(true) 

            let final = product.filter(a => a.sku !== "" )
            setproductList(final)
            setLoadedProduct(true)
        }
        if(valueVendor.data.length !== 0) {
            proceedProduct()
        }
        // eslint-disable-next-line
    }, [valueVendor.data]);

    return (
        <div>
            <Navbar/>
            <Container>
                <Box pt={2} pb={2} pl={2}>
                <Breadcrumbs aria-label="breadcrumb">
                    <Link color="inherit" href="/" >
                        Home
                    </Link>
                    <Link color="inherit" href="/my-template" >
                        Manage Template
                    </Link>
                    <Typography color="textPrimary">Create Template</Typography>
                </Breadcrumbs>
                </Box>
                <Box pt={2} pb={4} pl={2} pr={2} style={{ minHeight: 750 }}>
                    <Box pt={2} pb={2}>
                        <Typography color="textPrimary" variant="h5" style={{ fontWeight: 700 }}>Create Template</Typography>
                    </Box>
                    <Box pt={2}>
                        <Grid container spacing={3}>
                            <Grid item lg={3} md={3} sm={3} xs={6} >
                                <Typography color="textPrimary" variant="h6">Template Name </Typography>
                            </Grid>
                            <Grid item lg={9} md={9} sm={9} xs={6}>
                                <TextField
                                    // variant="outlined"
                                    style={{backgroundColor: 'white'}}
                                    required
                                    fullWidth
                                    size='small'
                                    id="name"
                                    label="Template Name"
                                    // value={valueCompany.name}
                                    name="name"
                                    onChange={(e) => setTemplateName(e.target.value)}
                                />
                            </Grid>
                        </Grid>
                        <Grid container spacing={3}>
                            <Grid item lg={3} md={3} sm={3} xs={6} >
                                <Typography color="textPrimary" variant="h6">Choose Vendor </Typography>
                            </Grid>
                            <Grid item lg={9} md={9} sm={9} xs={6}>
                                { !loadedVendor ?
                                <Skeleton variant="rect" height={40} />
                                :
                                <Select 
                                    placeholder="Search a vendor"
                                    options={loadedVendor && vendorList} 
                                    value={valueVendor.data}
                                    onChange={handleChangeVendor}
                                    style={{ textTransform: 'capitalize' }}
                                />
                                }
                            </Grid>
                        </Grid>
                        <Grid container spacing={3}>
                            <Grid item lg={3} md={3} sm={3} xs={6}>
                                <Typography color="textPrimary" variant="h6">Choose Product </Typography>
                            </Grid>
                            <Grid item lg={9} md={9} sm={9} xs={6}>
                                { !loadedProduct ?
                                <Skeleton variant="rect" height={40} />
                                :
                                <Select 
                                    placeholder="Search a Product"
                                    options={loadedProduct && productList} 
                                    value={valueProduct.data}
                                    onChange={handleChangeProduct}
                                />
                                }
                            </Grid>
                        </Grid>
                        <Grid container spacing={3}>
                            <Grid item lg={3} md={3} sm={3} xs={6}>
                                <Typography color="textPrimary" variant="h6">Choose SKU </Typography>
                            </Grid>
                            <Grid item lg={9} md={9} sm={9} xs={6}>
                                { !loadedSKU ?
                                <Skeleton variant="rect" height={40} />
                                :
                                <Select 
                                    placeholder="Search a SKU"
                                    options={loadedSKU && productSKUList} 
                                    value={valueSKU.data}
                                    onChange={handleChangeSKU}
                                />
                                }
                            </Grid>
                        </Grid>
                        
                    </Box>

                    { valueVendor.data.length !== 0 && valueProduct.data.length !== 0 ? 
                    <Box pt={4} pb={2}>
                        <Paper>
                            <Box p={3}>
                                <Box><Typography color="textPrimary" variant="h6" fontWeight={700}>{valueProduct.data.value} - ({valueSKU.data.value})</Typography></Box>
                                <Box pt={1}><Typography color="textPrimary" variant="body2">{valueVendor.data.label}</Typography></Box>
                                <Box pt={1}><Typography color="textPrimary" variant="body2">Quantity : {quantity}</Typography></Box>
                                <Box pt={2}><Typography color="textPrimary" variant="h6"  fontWeight={700}>Price : Rp. {parseInt(price).toLocaleString()}</Typography></Box>
                                <Box pt={1} style={{display:'flex', justifyContent: 'space-between'}}>
                                    <Box pr={2}><Button variant="contained" size="small" onClick={onClickReset}>Reset Item</Button></Box>
                                    <div style={{display: 'flex'}}>
                                        <div style={{display:'flex', paddingRight: 20}}>
                                            <Button 
                                                size="small"
                                                variant="outlined" 
                                                style={{backgroundColor: 'white'}} 
                                                onClick={onClickRemove}
                                                disabled={valueProduct.data.length !== 0 && quantity > 1 ? false : true}
                                            >-</Button>
                                            <Box 
                                                pl={2} 
                                                pr={4} 
                                                style={{ width: 10, fontSize: 20, fontWeight: 600}}
                                            >{quantity}</Box>
                                            <Button 
                                                size="small"
                                                variant="outlined" 
                                                style={{backgroundColor: 'white'}}  
                                                onClick={onClickAdd}
                                                disabled={valueProduct.data.length !== 0 ? false : true}
                                            >+</Button>
                                        </div>
                                        <Button variant="contained" color="primary" size="small" onClick={onClickToCart}>Add to Template</Button>
                                    </div>
                                </Box>
                            </Box>
                            
                        </Paper>
                    </Box>
                    : null }

                    { dataCart.length !== 0 ? 
                    <Box pt={4} pb={2}>
                        <TableCartTemplate 
                            data={dataCart}
                            setDataCart={setDataCart}
                            templateName={templateName}
                        />
                    </Box>
                    : null }
                    
                </Box>
            </Container>
            
            <Footer/>
        </div>
    )
}

export default CreateTemplate
