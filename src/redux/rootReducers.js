import { combineReducers } from 'redux';
import authReducer from './authentication/reducers';
import authReducerVendor from  './authenticationVendor/reducers'

const rootReducers = combineReducers({
  auth: authReducer,
  authVendor: authReducerVendor
});

export default rootReducers;
