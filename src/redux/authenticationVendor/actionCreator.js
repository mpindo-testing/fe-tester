import actions from './actions'; 
 
const { 
  loginBegin, 
  loginSuccess, 
  loginErr, 
  logoutBegin, 
  logoutSuccess, 
  logoutErr,
} = actions;

const login = (value) => {
  return async dispatch => { 
    try {
      dispatch(loginBegin());
      setTimeout(async() => {
        let user_registered = localStorage.getItem('user_registered') ? JSON.parse(localStorage.getItem('user_registered')) : null
        let users_list = localStorage.getItem('users_list') ? JSON.parse(localStorage.getItem('users_list')) : null

        if(user_registered !== null || users_list !== null) {
          if(value.email === user_registered.email && value.password === user_registered.password) {
            localStorage.setItem("role", user_registered.role)
            localStorage.setItem("name", user_registered.name)
            localStorage.setItem("logIn", true)
            dispatch(loginSuccess(true))
          } else if(users_list.find(val => val.email === value.email)) {
            localStorage.setItem("role", users_list.find(val => val.email === value.email).role)
            localStorage.setItem("name", users_list.find(val => val.email === value.email).name)
            localStorage.setItem("logIn", true)
            dispatch(loginSuccess(true))
          } else {
            dispatch(loginErr("Email or passsword is wrong"));
          }
        } else {
          dispatch(loginErr("Email or passsword is wrong"));
        }


        // if(value.email === "demo@gmail.com" && value.password === "demo123" ) {
        //   localStorage.setItem("role", "user")
        //   localStorage.setItem("name", "Demo User")
        //   localStorage.setItem("logIn", true)
        //   dispatch(loginSuccess(true))
        // } else if (value.email === "admin@gmail.com" && value.password === "demo123"  ) {
        //   localStorage.setItem("role", "admin")
        //   localStorage.setItem("name", "Demo Admin")
        //   localStorage.setItem("logIn", true)
        //   dispatch(loginSuccess(true))
        // } else {
        //   dispatch(loginErr("Email or passsword is wrong"));
        // }
      }, 1000);
    } catch (err) {
      dispatch(loginErr(err));
    }
  };
};

const logOut = () => {
  return async dispatch => {
    try {
      dispatch(logoutBegin());
      localStorage.removeItem('logIn')
      localStorage.removeItem('role')
      window.location.href = "/login"
      dispatch(logoutSuccess(null));
    } catch (err) {
      dispatch(logoutErr(err));
    }
  };
};


export { login, logOut };
