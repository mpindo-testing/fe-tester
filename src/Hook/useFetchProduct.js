import { useState, useEffect } from 'react'
import Axios from 'axios'

function useFetchProduct() {

    const [products, setProducts] = useState([])
    const [loaded, setLoaded] = useState(false)

    useEffect(() => { 
        const getProducts = async () => {      
            try {    
              const response = await Axios({
                url: 'https://ap-southeast-1.aws.webhooks.mongodb-realm.com/api/client/v2.0/app/luxofood-injections-xgzns/service/ProductsManagement/incoming_webhook/getProducts',
                method: 'get'
            })
            if(response) {
              setProducts(response.data)
              setLoaded(true)
            }       
          } catch (error) {
              console.log(error)
          }        
        }
        getProducts()
    
    }, []);

    return { products, loaded }
}

export default useFetchProduct
