import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import BrowseProduct from './pages/BrowseProduct';
import CompanyDetail from './pages/CompanyDetails';
import CreatePR from './pages/CreatePR';
import CreateTemplate from './pages/CreateTemplate';
import Dashboard from './pages/Dashboard';
import Home from './pages/Home';
import Login from './pages/Login';
import ManagePO from './pages/ManagePO';
import ManagePR from './pages/ManagePR.js';
import ManageTemplate from './pages/ManageTemplate';
import ProductPage from './pages/ProductPage';
import Register from './pages/Register';
import UpdatePR from './pages/UpdatePR';
import UpdateTemplate from './pages/UpdateTemplate';
import UserManagement from './pages/UserManagement';
import CreateDeliveryNote from './pages/vendors/createDeliveryNote';
import CreateProduct from './pages/vendors/createProduct';
import LoginVendor from './pages/vendors/LoginVendor';
import RegisterVendor from './pages/vendors/RegisterVendor';
import ViewDetailOrders from './pages/ViewDetailOrders';
import ViewDetailPurchase from './pages/ViewDetailPurchase';
import ProtectedRoute from './utils/protectedRoute';

function AppRoute() {
  return (
    <div>
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/register" component={Register} />
          <Route exact path="/product/:id" component={ProductPage} />
          <Route exact path="/vendor" component={LoginVendor} />
          <Route exact path="/vendor/register" component={RegisterVendor} />
          <ProtectedRoute exact path="/dashboard" component={Dashboard} />
          <ProtectedRoute exact path="/browse" component={BrowseProduct} />
          <ProtectedRoute exact path="/manage-orders" component={ManagePO} />
          <ProtectedRoute exact path="/manage-purchases" component={ManagePR} />
          <ProtectedRoute exact path="/my-template" component={ManageTemplate} />
          <ProtectedRoute exact path="/create-purchases" component={CreatePR} />
          <ProtectedRoute exact path="/create-template" component={CreateTemplate} />
          <ProtectedRoute exact path="/update-purchase/:id" component={UpdatePR} />
          <ProtectedRoute exact path="/update-template/:id" component={UpdateTemplate} />
          <ProtectedRoute exact path="/view-purchase/:id" component={ViewDetailPurchase} />
          <ProtectedRoute exact path="/view-orders/:id" component={ViewDetailOrders} />
          <ProtectedRoute exact path="/company-detail" component={CompanyDetail} />
          <ProtectedRoute exact path="/user-management" component={UserManagement} />
          <ProtectedRoute exact path="/vendor/delivery-note" component={CreateDeliveryNote} />
          <Route exact path="*" component={Home} />   
        </Switch>
      </BrowserRouter>
    </div>
  )
}

export default AppRoute;
