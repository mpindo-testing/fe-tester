import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './asset/css/style.css'
import AppRoute from './AppRoute';
import reportWebVitals from './reportWebVitals';
import { Provider } from 'react-redux';
import store from './redux/store';
import Bugsnag from '@bugsnag/js'
import BugsnagPluginReact from '@bugsnag/plugin-react'

// Bugsnag.start({
//   apiKey: '343213f49aadc853875be645dfd1f168',
//   plugins: [new BugsnagPluginReact()]
// })

// const ErrorBoundary = Bugsnag.getPlugin('react').createErrorBoundary(React)

// Bugsnag.notify(new Error('Test error'))

ReactDOM.render(
  // <ErrorBoundary>
    <Provider store={store}>
      <AppRoute />
    </Provider>,
  // </ErrorBoundary>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
