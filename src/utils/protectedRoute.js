import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';

const ProtectedRoute = ({component: Component, ...rest}) => {    
    const isLoggedIn = useSelector(state => state.auth.login);

    const isAuthenticated = () => {
        if(typeof window == 'undefined') {
            return false
        }
        if(isLoggedIn) {
            return isLoggedIn
        } else {
            return false
        }
    }


    return (
        <Route 
            {...rest} 
            render={props => {
                if(isAuthenticated())  {
                    return <Component {...props} />
                }
                else {
                    return <Redirect 
                            to={{
                                pathname: "/login",
                                state: { from: props.location }
                            }}
                        />
                }
            
            } }
        />
)

}

export default ProtectedRoute;