import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Typography, Box, Button, Grid } from '@material-ui/core';
import { groupBy } from 'lodash';
import moment from 'moment';

const useStyles = makeStyles({
  table: {
    minWidth: 500,
  },
});

export default function TablePuchaseOrders({ loaded, dataPuchaseReq }) {
  const classes = useStyles();

  function checkValidate (data) {
    let vendor = false
    const convert_array = Object.entries(groupBy(data, 'vendor_name'));
    if(convert_array.length > 1) {
      vendor = true
    }

    return vendor;

  }


  return (
    <Box pr={2} pl={2} pt={2} mb={4}>
        <Box pb={4}>
            <Grid container justifyContent="space-between">
              <Grid item>
                <Typography component="h1" variant="h5">
                    List Purchase Orders 
                </Typography>
              </Grid>
            </Grid>
        </Box>
        { dataPuchaseReq.length !== 0 ? 
        <TableContainer component={Paper}>
          <Table className={classes.table} aria-label="simple table">
              <TableHead>
              <TableRow >
                  <TableCell style={{fontWeight: 700}}>PO ID</TableCell>
                  <TableCell style={{fontWeight: 700}}>DATE</TableCell>
                  <TableCell style={{fontWeight: 700}}>AUTHOR</TableCell>
                  <TableCell style={{fontWeight: 700}}>VENDOR</TableCell>
                  <TableCell style={{fontWeight: 700}}>TOTAL PRICE</TableCell>
                  <TableCell style={{fontWeight: 700}}>STATUS</TableCell>
                  <TableCell style={{fontWeight: 700}}>ACTION</TableCell>
              </TableRow>
              </TableHead>
              <TableBody>
              {loaded && dataPuchaseReq.map((row, i) => (
                  <TableRow key={i}>
                  <TableCell  component="th" scope="row">
                      {row.id}
                  </TableCell>
                  <TableCell component="th" scope="row">
                      {moment(row.date).format('l')}
                  </TableCell>
                  <TableCell component="th" scope="row">
                      {row.author}
                  </TableCell>
                  <TableCell component="th" scope="row">
                  { checkValidate(row.data) ? "Multi Vendor" : row.data[0].vendor_name }
                  </TableCell>
                  <TableCell>Rp. {row.total_price.toLocaleString()}</TableCell>
                  <TableCell>{row.status}</TableCell>
                  <TableCell>
                      <Grid container>
                        <Grid item>
                          <Box pr={1}>
                            <Button variant="contained" color="primary" size="small" href={`/view-orders/${row.id}`}>
                                View Detaiil
                            </Button>
                          </Box>
                        </Grid>
                        
                      </Grid> 
                  </TableCell>
                  </TableRow>
              ))}
              </TableBody>
          </Table>
        </TableContainer>
        : 
        <Paper>
          <Box p={2}>
            You dont have any Purchase Orders! 
          </Box>
        </Paper>
        }
    </Box>
  );
}