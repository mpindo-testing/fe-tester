import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { Button, Typography, Box, Container, Popover } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux'
import { logOut } from './../../redux/authenticationVendor/actionCreator'
import { Link } from 'react-router-dom'
import AccountCircle from '@material-ui/icons/AccountCircle';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
    marginBottom: 10
  },
  menuNav: {
      marginLeft: 50,
      display: 'flex',
      color: 'white'
  },
  rightNav: {
      display: 'flex'
  },
  root: {
    width: '100%',
    minWidth: 160,
    backgroundColor: theme.palette.background.paper,
  },
  
}));

export default function NavbarVendor() {
  const classes = useStyles();
  const dispatch = useDispatch()

  const auth = useSelector(state => state.auth)

  // dropdown menu 
  const [anchorEl, setAnchorEl] = React.useState(null);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const open = Boolean(anchorEl);
  const id = 2 ? 'simple-popover' : undefined;


  function ListItemLink(props) {
    return <ListItem button component="a" {...props} />;
  }

  return (
    <div className={classes.grow}>
      <AppBar position="static" color="secondary">
        <Container>
        <Toolbar style={{marginLeft: 0, marginRight: 0}}>
          <a href="/dashboard">
            <Typography variant="h5" style={{color: 'white'}}>
              MPI - VENDOR
            </Typography>
          </a>
          {/* <div className={classes.menuNav}>
            <Box pr={2}>
                <Typography variant="h6">
                    About Us
                </Typography>
            </Box>
            <Box>
                <Typography variant="h6">
                Contact Us
                </Typography>
            </Box>
          </div> */}
          <div className={classes.grow} />

          { auth.login ? 
          <div className={classes.rightNav}>
            {/* <div>
              <Box pr={2}>
              <Link to="/browse">
                <Button color="default" variant='contained' size="small">
                    Browse Product
                </Button>
              </Link>
              </Box>
            </div> */}
            <div style={{display: 'flex', cursor: 'pointer'}} onClick={handleClick}>
              <Box> <AccountCircle/>  </Box>
              <Box pl={1}> {localStorage.getItem('name')}</Box>
              <Box pl={1}> <ArrowDropDownIcon/></Box>
            </div>

            {/* Dropdown Menu */}
            <Popover
              id={id}
              open={open}
              anchorEl={anchorEl}
              onClose={handleClose}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              className="popper-style"
            >
              <Box>
                <div className={classes.root}>
                  <List component="nav" aria-label="secondary mailbox folders">
                    <ListItemLink  href="vendor/my-order">
                      <ListItemText primary="My Orders" />
                    </ListItemLink>
                    <ListItemLink  href="/vendor/company-detail">
                      <ListItemText primary="Company Detail" />
                    </ListItemLink>
                    <ListItemLink href="/vendor/user-management">
                      <ListItemText primary="User Management" />
                    </ListItemLink>
                    <ListItem button onClick={() => dispatch(logOut())}>
                      <ListItemText primary="Sign Out" />
                    </ListItem>
                   
                  </List>
                </div>
              </Box>
            </Popover>
           
          </div>
          :
          <div className={classes.rightNav}>
            <Box pr={2}>
              <Link to="/vendor">
                <Button color="secondary" variant='contained'>
                    Login
                </Button>
              </Link>
            </Box>
            <Box>
              <Link to="/vendor/register">
                <Button color="inherit" variant='contained'>
                    Register
                </Button>
              </Link>
            </Box>
          </div>
          }
          
        </Toolbar>
        </Container>
      </AppBar>
    </div>
  );
}