import React from 'react';
import { makeStyles,  } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Box, Button, Grid,  } from '@material-ui/core';
import moment from 'moment';


const useStyles = makeStyles({
  table: {
    // width: '100%'
    // display:'block',
    // overflowX:'auto'
  },
  absolute: {
    position: 'absolute',
    bottom: 30,
    right: 50,
  },
});


export default function TableItem({ loaded, dataUsers }) {
  const classes = useStyles();


  return (
    <Box pr={2} pl={2} mb={4}>
        <TableContainer >
          <Table className={classes.table} aria-label="simple table"  > 
              <TableHead>
              <TableRow >
                  <TableCell style={{fontWeight: 700}}>NO</TableCell>
                  <TableCell style={{fontWeight: 700}}>NAME</TableCell>
                  <TableCell style={{fontWeight: 700}}>CATEGORY</TableCell>
                  <TableCell style={{fontWeight: 700}}>BRAND</TableCell>
                  <TableCell style={{fontWeight: 700}}>QTY</TableCell>
                  <TableCell style={{fontWeight: 700}}>PRICE</TableCell>
                  <TableCell style={{fontWeight: 700}}>STATUS</TableCell>
              </TableRow>
              </TableHead>
              <TableBody >
              { [1,2,3,4].map((row, i) => (
                <TableRow key={i}>
                  <TableCell>{i + 1}</TableCell>
                  <TableCell component="th" scope="row">
                    Paysan Breton - Butter Unsalted (200G)
                  </TableCell>
                  <TableCell component="th" scope="row">
                    Wine
                  </TableCell>
                  <TableCell component="th" scope="row">
                    Gallop
                  </TableCell>
                  <TableCell component="th" scope="row">
                    12
                  </TableCell>
                  <TableCell component="th" scope="row">
                    RP. 55,000	
                  </TableCell>
                  <TableCell component="th" scope="row">
                    -
                  </TableCell>
                  
                </TableRow>

              ))}
              </TableBody>
          </Table>
        </TableContainer>
    </Box>
  );
}