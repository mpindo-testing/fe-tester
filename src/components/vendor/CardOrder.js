import React from 'react'
import { Box, Paper, Grid, Button } from '@material-ui/core'
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import TableItem from './TableItem';
import { Link } from 'react-router-dom'

function CardOrder() {

    // const 
    return (
    <div>
        {[1,2,3].map((data) => (
            <Box pb={2} pt={2} key={data}>
                <Paper elevation={4}>
                    <Paper>
                    <Grid container spacing={2} justifyContent="space-between">
                        <Grid item>
                           <Box fontWeight="bold" pl={2}> Order ID : 12312312</Box>
                           <Box fontWeight="normal" pl={2} fontSize={13}> Date : 17 September 2021</Box>
                        </Grid>
                        <Grid item>
                            <Grid container spacing={2}>
                                <Grid item>
                                    <Box pt={1} pr={2}>
                                        <Link  to="/vendor/delivery-note">
                                        <Button size="small" color="primary" variant="contained">
                                            Create Delivery Note
                                        </Button>
                                        </Link>
                                    </Box>
                                </Grid>
                                {/* <Grid item>
                                    <Box pt={1} pr={2}>
                                        <Button size="small" color="primary" variant="contained">
                                            Accept Order
                                        </Button>
                                    </Box>
                                </Grid> */}
                            </Grid>
                        </Grid>
                    </Grid>
                    </Paper>
                    <Box p={2}>
                        <Grid container spacing={3} >
                            <Grid item xl={6} lg={6} md={6} sm={6} xs={12}>
                                <Box pt={2} fontWeight="bold" style={{display:'flex'}}>
                                    <Box pr={1}>PT Luxofood Indonesia </Box>
                                    <Button size="small" color="primary" variant="outlined">
                                        Message
                                    </Button>
                                </Box> 
                                <Box pt={2} fontWeight="normal" fontSize={14}>
                                    <p style={{width: '80%'}}>Jl Let Jend S. Parman Kav 28 APL Tower, 16th Floor T9, RT.3/RW.5, Tj. Duren Sel., Kec. Grogol petamburan, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11470</p>
                                </Box>
                            </Grid>
                            <Grid item xl={6} lg={6} md={6} sm={6} xs={12}>
                                <Box fontWeight="bold" pt={2}> Total Items : 20</Box>
                                <Box fontWeight="bold" pt={1}> Total Price : Rp. 45.000.000</Box>
                                <Box fontWeight="bold" pt={1}> Payment Terms : 2 weeks</Box>
                                <Box pt={2}>
                                    <Grid container spacing={2}>
                                        <Grid item>
                                            <Button size="small" color="secondary" variant="contained">
                                                Print Pick & Pack
                                            </Button>
                                        </Grid>
                                        <Grid item>
                                            <Button size="small" color="default" variant="outlined" disabled>
                                                Print Invoice
                                            </Button>
                                        </Grid>
                                        <Grid item>
                                            <Button size="small" color="primary" variant="outlined" disabled>
                                                Print Faktur Pajak
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </Box>
                            </Grid>
                        </Grid>
                        
                    </Box>

                    <Box pt={2} >
                        <Paper elevation={3}>
                            <Accordion>
                                <AccordionSummary
                                expandIcon={<ExpandMoreIcon />}
                                aria-controls="panel1a-content"
                                id="panel1a-header"
                                >
                                    <Box fontWeight="bold">Detail All Items (4)</Box>
                                </AccordionSummary>
                                <AccordionDetails>
                                    
                                <Box style={{ width: '100%'}}>
                                    <TableItem/>
                                </Box>
                                </AccordionDetails>
                            </Accordion>
                        </Paper>
                    </Box>
                </Paper>
            </Box>
        ))
        }
    </div>
    )
}

export default CardOrder
