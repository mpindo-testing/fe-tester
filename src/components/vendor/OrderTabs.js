import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { Grid, Select, MenuItem, InputLabel, FormControl, FilledInput, Paper, InputAdornment, IconButton } from '@material-ui/core';
import { DateRange  } from 'react-date-range';
import Visibility from '@material-ui/icons/Visibility';
import { addDays } from 'date-fns';
import 'react-date-range/dist/styles.css'; // main style file
import 'react-date-range/dist/theme/default.css'; // theme css file
import ButtonSearchOrder from './ButtonSearchOrder';
import CardOrder from './CardOrder';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    // marginBottom: 50
  },
}));

export default function OrderTabs() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const [state, setState] = useState([
    {
      startDate: new Date(),
      endDate: addDays(new Date(), 1),
      key: 'selection'
    }
  ]);

console.log(state, 'state')
  const handleRange = (date) => {
    console.log(date, 'date')
  }

  return (
    <>
    <div className={classes.root}>
      <AppBar position="static" color="primary">
        <Tabs value={value} onChange={handleChange} aria-label="simple tabs example">
          <Tab label="New Orders" {...a11yProps(0)} />
          <Tab label="Picked & Pack" {...a11yProps(1)} />
          <Tab label="Ready to Ship" {...a11yProps(2)} />
          <Tab label="Shipped" {...a11yProps(3)} />
          <Tab label="Delivered" {...a11yProps(4)} />
          <Tab label="Delayed" {...a11yProps(4)} />
        </Tabs>
        <div className="search-bar">
          <Box p={3}>
            <Grid container spacing={2}>
              <Grid item xl={6} lg={6} md={6} sm={6} xs={12}>
                <ButtonSearchOrder/>
              </Grid>
              <Grid item xl={3} lg={3} md={3} sm={6} xs={12}>
                <FormControl variant="filled" fullWidth>
                  <InputLabel>Sort By</InputLabel>
                  <Select
                    labelId="demo-simple-select-outlined-label"
                    id="demo-simple-select-outlined"
                    // value={age}
                    // onChange={handleChange}
                    fullWidth
                    style={{height: 50}}
                  >
                    <MenuItem value="Newest">Newest</MenuItem>
                    <MenuItem value="Oldest">Oldest</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xl={3} lg={3} md={3} sm={6} xs={12}>
                <FormControl variant="filled">
                  <InputLabel htmlFor="filled-adornment-password">Date Range</InputLabel>
                  <FilledInput
                    id="filled-adornment-password"
                    // type={values.showPassword ? 'text' : 'password'}
                    // value={values.password}
                    // onChange={handleChange('password')}
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          // onClick={handleClickShowPassword}
                          // onMouseDown={handleMouseDownPassword}
                          edge="end"
                        >
                          <Visibility /> 
                        </IconButton>
                      </InputAdornment>
                    }
                  />
                </FormControl>
                {/* <DateRange 
                    onChange={item => setState([item.selection])}
                    months={1}
                    ranges={state}  
                    singleDateRange={true}
                /> */}
              </Grid>

            </Grid>
          </Box>
        </div>
      </AppBar>
      
    </div>
    <Box mt={2} mb={4}>
      <Paper style={{ backgroundColor: '#fff' }}>
      <TabPanel value={value} index={0}>
        <CardOrder/>
      </TabPanel>
      <TabPanel value={value} index={1}>
        Picked and pack
      </TabPanel>
      <TabPanel value={value} index={2}>
        Ready to Ship
      </TabPanel>
      <TabPanel value={value} index={3}>
        Shipped
      </TabPanel>
      <TabPanel value={value} index={4}>
        Delivered
      </TabPanel>

      </Paper>
    </Box>
    </>
  );
}
