import React, { useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import CircularProgress from '@material-ui/core/CircularProgress';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Alert from '@material-ui/lab/Alert';
import Box from '@material-ui/core/Box';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { useDispatch, useSelector } from 'react-redux'
import { login } from '../../redux/authenticationVendor/actionCreator'

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(7),
    paddingTop: 100,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    height: 800
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function SignInVendor() {
  const classes = useStyles();

  const dispatch = useDispatch()
  const auth = useSelector(state => state.auth)

  const [valueLogin, setValueLogin] = useState({
      email: "",
      password: "", 
  });


  const onChangeValue = (e) => {
    setValueLogin({...valueLogin, 
        [e.target.name] : e.target.value
    })
  }

  const onClickLogin = (e) => {
    e.preventDefault()
    dispatch(login(valueLogin))
  }

  const redirectHome = () => {
      if(auth.login) {
          window.location.href = "/"
      }
  }

  return (
    <Container component="main" maxWidth="xs">
    {redirectHome()}
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign In for Vendor
        </Typography>
        <form className={classes.form} validate="true" onSubmit={onClickLogin}>
          <TextField
            variant="filled"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            value={valueLogin.email}
            name="email"
            onChange={onChangeValue}
          />
          <TextField
            variant="filled"
            margin="normal"
            required
            fullWidth
            name="password"
            value={valueLogin.password}
            label="Password"
            type="password"
            id="password"
            onChange={onChangeValue}

          />

          <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label="Remember me"
          />
          { auth.error !== null ?
          <Box pt={1} pb={1}>
          <Alert severity="error">{auth.error}</Alert>
          </Box> : null }
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Sign In {auth.loading ? <CircularProgress size={20} color="inherit" style={{marginLeft: 20}} /> : null}
          </Button>
        </form>
      </div>
    </Container>
  );
}