import React, {useState, useEffect}  from 'react';
import { makeStyles,  } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Box, Button, Grid, TextField  } from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';
import moment from 'moment';
import DataTable from 'react-data-table-component';
import uuid from 'react-uuid'
import { find, groupBy, remove } from 'lodash'

const useStyles = makeStyles({
  table: {
    // width: '100%'
    // display:'block',
    // overflowX:'auto'
  },
  absolute: {
    position: 'absolute',
    bottom: 30,
    right: 50,
  },
});

const columns = [
  {
    name: 'ITEM ID',
    selector: row => row.id,
  },
  {
    name: 'NAME',
    selector: row => row.name,
    width: "200px",
  },
  {
    name: 'CATEGORY',
    selector: row => row.category,
  },
  {
    name: 'BRAND',
    selector: row => row.brand,
  },
  {
    name: 'QTY',
    cell: (row) => (
      <TextField 
        defaultValue={row.qty}
        variant="outlined"
        size="small"
        type="number"
        style={{ width: 90, height: 40}}
      />
    )
  },
  {
    name: 'PRICE',
    cell: (row) => (
      <TextField 
        defaultValue={row.price}
        variant="outlined"
        size="small"
        type="number"
        style={{ width: 150, height: 40}}
      />
    )
  },
  {
    name: 'NOTE',
    width: "200px",
    cell: (row) => (
      <TextField 
        variant="outlined"
        size="small"
        type="text"
        style={{ width: 150, height: 40}}
      />
    )
  },
  {
    name: 'ACTION',
    cell:(row)=> (
      <Button color="secondary" variant="contained" >
          OOS
      </Button>
    )
  },
];

export default function TableItemDelivery({ data, loaded, onClickUpdateData }) {
  const classes = useStyles();

  const [dataItems, setDataItems] = useState([]);
  const [loadDone, setLoadDone] = useState(false);
  const [selectData, setSelectedData] = useState([]);
  const [toggledClearRows, setToggledClearRows] = useState(false);


  console.log(selectData, 'selet dat')
  // const newpackageID = uuid()

  const onClickCreatePackage = () => {
    const newpackageID = uuid()

    console.log(newpackageID, 'newpackageID')

    if(selectData.length !== 0) {
      let copy = [...data]
      let select = [...selectData]

      console.log(copy, 'copyy')

      for(let k = 0; k < copy.length; k ++) {
        for( let m = 0; m < select.length ; m ++ ) {
          if(copy[k].id === select[m].id) {
            copy.find(val =>  val.id === select[m].id).id_package = newpackageID;
          } 
        }
      }
      setSelectedData([])
      setToggledClearRows(!toggledClearRows)
      console.log(copy, 'copyy alkhir')
      onClickUpdateData(copy)
    }
  }

  useEffect(() => {
    if(loaded) {
      setTimeout(() => {
        let copy = [...data]
      
        const convert_array = Object.entries(groupBy(copy, 'id_package'));
        var final_data = convert_array.map(function(key) { 
          return { 
            id_package: key[0], 
            items: key[1] 
          }; 
        });
        
        setDataItems(final_data)
        setLoadDone(true)
      }, 1000);
    }
  }, [loaded, data]);

  return (
    <Box mb={4}>
      { !loadDone ? 
      <Box>
          <Box pb={1}><Skeleton  variant="rect" height={50} /></Box>
          <Box><Skeleton  variant="rect" height={400} /></Box>
      </Box>
      :
      <>
      <Box pt={2} >
          <Button color="default" variant="contained" onClick={() => onClickCreatePackage()}>
              Create a Separate Package
          </Button>
      </Box>
      { loadDone && dataItems.map((data, i) => (
        <Box pt={2} pb={2} key={i}>
          <div style={{ width: '100%' }}>
            <Box pb={1}>
              <Paper>
                <Box p={2} fontWeight="bold" fontSize={17}>
                  Package ID : {data.id_package}
                </Box>
              </Paper>
            </Box>
            <DataTable
                columns={columns}
                data={data.items}
                selectableRows
                onSelectedRowsChange={data =>
                  setSelectedData(data.selectedRows)
                }
                clearSelectedRows={toggledClearRows}
                pagination 
            />
          </div>
          <Box pt={2} style={{ display: 'flex', justifyContent: 'flex-end' }}>
            <Button color="primary" variant="contained" >
                  Submit Delivery Note
              </Button>
          </Box>
        </Box>
      )) }
      </>
      }
    </Box>
  );
}