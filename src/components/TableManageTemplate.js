import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Typography, Box, Button, Grid, Link } from '@material-ui/core';
import { groupBy } from 'lodash'
import moment from 'moment'
import { useHistory } from 'react-router-dom'

const useStyles = makeStyles({
  table: {
    minWidth: 500,
  },
});

export default function TableManageTemplate({ loaded, dataPuchaseReq }) {
  const classes = useStyles();
  const history = useHistory()

  function checkValidate (data) {
    let vendor = false
    const convert_array = Object.entries(groupBy(data, 'vendor_name'));
    if(convert_array.length > 1) {
      vendor = true
    }

    return vendor;

  }

  const onClickPRFromTemplate = (value) => {
    history.push({
      pathname: `/create-purchases`,
      state: {  
        product: value.data
      }
    })
  }


  return (
    <Box pr={2} pl={2} pt={2} mb={4}>
        <Box pb={4}>
            <Grid container justifyContent="space-between">
              <Grid item>
                <Typography component="h1" variant="h5">
                    My Template 
                </Typography>
              </Grid>
              { localStorage.getItem('role') === "requester" || localStorage.getItem('role') === "admin" ||  localStorage.getItem('role') === "owner"  ?
              <Grid item>
                <Link href="/create-template">
                  <Button variant="contained" color="secondary" size="medium">
                      Create New Template
                  </Button>
                </Link>
              </Grid> : null }
            </Grid>
        </Box>
        { dataPuchaseReq.length !== 0 ? 
        <TableContainer component={Paper}>
          <Table className={classes.table} aria-label="simple table">
              <TableHead>
              <TableRow >
                  <TableCell style={{fontWeight: 700}}>NAME</TableCell>
                  <TableCell style={{fontWeight: 700}}>CREATED DATE</TableCell>
                  <TableCell style={{fontWeight: 700}}>AUTHOR</TableCell>
                  <TableCell style={{fontWeight: 700}}>VENDOR</TableCell>
                  <TableCell style={{fontWeight: 700}}>TOTAL PRICE</TableCell>
                  {/* <TableCell style={{fontWeight: 700}}>STATUS</TableCell> */}
                  <TableCell style={{fontWeight: 700}}>ACTION</TableCell>
              </TableRow>
              </TableHead>
              <TableBody>
              {loaded && dataPuchaseReq.map((row, i) => (
                  <TableRow key={i}>
                  <TableCell component="th" scope="row">
                      {row.name}
                  </TableCell>
                  <TableCell component="th" scope="row">
                      {moment(row.date).format('l')}
                  </TableCell>
                  <TableCell component="th" scope="row">
                      {row.author}
                  </TableCell>
                  <TableCell component="th" scope="row">
                    { checkValidate(row.data) ? "Multi Vendor" : row.data[0].vendor_name }
                  </TableCell>
                  <TableCell>Rp. {row.total_price.toLocaleString()}</TableCell>
                  <TableCell>
                      <Grid container>
                        <Grid item>
                          <Box pr={1}>
                            <Button 
                              disabled={ localStorage.getItem('role') === "accounting" || localStorage.getItem('role') === "approver" ? true : false } 
                              variant="contained" color="default" size="small" href={`/update-template/${row.id}`}>
                              Update
                            </Button>
                          </Box> 
                        </Grid>
                        <Grid item>
                          <Box pr={1} >
                            <Button 
                              disabled={ localStorage.getItem('role') === "accounting" || localStorage.getItem('role') === "approver" ? true : false } 
                              variant="contained" color="primary" size="small"
                              onClick={() => onClickPRFromTemplate(row)}>
                                Create Purchase Request
                            </Button>
                          </Box> 
                        </Grid>
                      </Grid> 

                  </TableCell>
                  </TableRow>
              ))}
              </TableBody>
          </Table>
        </TableContainer>
        : 
        <Paper>
          <Box p={2}>
            You dont have any Template yet.
          </Box>
        </Paper>
        }
    </Box>
  );
}