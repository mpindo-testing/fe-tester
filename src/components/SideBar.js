import React, { useEffect, useState } from 'react'
import { Box, Paper, Button, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import ListSubheader from '@material-ui/core/ListSubheader';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  nested: {
    paddingLeft: theme.spacing(4),
  }
}));

function SideBar({ 
    dataVendor, loadedVendor,
    loadedCategory, dataCategory,
    onClickFilter, onClickReset, clickReset 
}) {

    const classes = useStyles();
    const [dataSideList, setDataSideList] = useState([]);
    const [loadSide, setloadSide] = useState(false);

    useEffect(() => {

        if(loadedVendor && loadedCategory) {
            let data = [
                {
                    id: 1,
                    name : "Category",
                    open : false,
                    data : dataCategory
                },
                {
                    id: 2,
                    name : "Vendor",
                    open : false,
                    data : dataVendor
                }
            ]
            setDataSideList(data)
            setloadSide(true)
        }
        // eslint-disable-next-line
    }, [loadedVendor, loadedCategory]);

    const handleClick = (id, open) => {
        let data = [...dataSideList] 
        if(data.find(value => value.id === id)) {
            if(!open) {
                data.find(value => value.id === id).open = true
                setDataSideList(data)
            } else {
                data.find(value => value.id === id).open = false
                setDataSideList(data)
            }
        }
    };

    return (
        <div>
            <Paper className="paper-div">
                <Box p={1}>
                    <List
                        component="nav"
                        aria-labelledby="nested-list-subheader"
                        subheader={
                            <ListSubheader>
                                <Typography style={{ fontWeight: 500, fontSize: 20, paddingBottom: 10 }}>Filter</Typography>
                            </ListSubheader>
                        }
                        className={classes.root}
                    >
                    { loadSide && dataSideList.map((data, key) => (
                        <div key={key}>
                            <ListItem button onClick={() => handleClick(data.id, data.open)}>
                                <ListItemText primary={data.name} />
                                {data.open ? <ExpandLess /> : <ExpandMore />}
                            </ListItem>
                            <Collapse in={data.open} timeout="auto" unmountOnExit>
                                <List component="div" disablePadding>
                                    { data.data.map((name, key) => (
                                        <ListItem button className={classes.nested} key={key} onClick={() => onClickFilter(name.name, data.name)}>
                                            <ListItemText primary={name.name} />
                                        </ListItem>
                                    )) }
                                </List>
                            </Collapse>
                        </div>
                    )) }
                    </List>
                    <Box pt={2} pb={2} pl={2}>
                        <Button color="default" size="small" variant="outlined" onClick={() => onClickReset(!clickReset)}>Reset Filter</Button>
                    </Box>
                </Box>
            </Paper>
        </div>
    )
}

export default SideBar
