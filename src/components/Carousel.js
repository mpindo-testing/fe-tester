import { Container } from '@material-ui/core'
import React from 'react'
import { Carousel } from 'react-bootstrap'
import slide from './../asset/img/slide.jpg'

function CarouselComponent() {
    return (
        <div>
            <Container>
            <Carousel>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src={slide}
                    alt="First slide"
                    />
                    <Carousel.Caption>
                    <h3>BENEFIT</h3>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src={slide}
                    alt="Second slide"
                    />

                    <Carousel.Caption>
                    <h3>PROCESS</h3>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src={slide}
                    alt="Third slide"
                    />

                    <Carousel.Caption>
                    <h3>PAYMENT TERMS</h3>
                    </Carousel.Caption>
                </Carousel.Item>
                </Carousel>
            </Container>
        </div>
    )
}

export default CarouselComponent
