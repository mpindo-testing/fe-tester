import React, { useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import CircularProgress from '@material-ui/core/CircularProgress';
import TextField from '@material-ui/core/TextField';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { useDispatch, useSelector } from 'react-redux'
import { login } from '../redux/authentication/actionCreator'

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(7),
    paddingTop: 100,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    height: 800
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function SignUp() {
  const classes = useStyles();

  const dispatch = useDispatch()
  const auth = useSelector(state => state.auth)

  const [valueLogin, setValueLogin] = useState({
      email: "",
      password: "", 
      company_name: "",
      name: ""
  });


  const onChangeValue = (e) => {
    setValueLogin({...valueLogin, 
        [e.target.name] : e.target.value
    })
  }

  const onClickRegister = (e) => {
    e.preventDefault()
    let user_owner = {
      email: valueLogin.email,
      password: valueLogin.password, 
      name: valueLogin.name,
      company_name : valueLogin.company_name,
      role: 'owner'
    }

    let user = {
      email: valueLogin.email,
      password: valueLogin.password, 
      name: valueLogin.name,
      role: 'owner',
      date : new Date()
    }

    let users_list = []
    users_list.push(user)

    localStorage.setItem('user_registered', JSON.stringify(user_owner))
    localStorage.setItem('users_list', JSON.stringify(users_list))
    localStorage.setItem('first_login', true)
    dispatch(login(user_owner))

  }

  const redirectHome = () => {
      if(auth.login) {
          window.location.href = "/company-detail"
      }
  }

  return (
    <Container component="main" maxWidth="xs">
    {redirectHome()}
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign Up
        </Typography>
        <form className={classes.form} validate="true" onSubmit={onClickRegister}>
          <TextField
            variant="filled"
            margin="normal"
            required 
            fullWidth
            id="name"
            label="Name"
            value={valueLogin.name}
            name="name"
            onChange={onChangeValue}
          />
          <TextField
            variant="filled"
            margin="normal"
            required 
            fullWidth
            id="company_name"
            label="Company Name"
            value={valueLogin.company_name}
            name="company_name"
            onChange={onChangeValue}
          />
          <TextField
            variant="filled"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            value={valueLogin.email}
            name="email"
            onChange={onChangeValue}
          />
          <TextField
            variant="filled"
            margin="normal"
            required
            fullWidth
            name="password"
            value={valueLogin.password}
            label="Password"
            type="password"
            id="password"
            onChange={onChangeValue}

          />

          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Register {auth.loading ? <CircularProgress size={20} color="inherit" style={{marginLeft: 20}} /> : null}
          </Button>
        </form>
      </div>
    </Container>
  );
}