import React from 'react';
import { makeStyles,  } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Box, Button, Grid,  } from '@material-ui/core';
import moment from 'moment';


const useStyles = makeStyles({
  table: {
    minWidth: 500,
  },
  absolute: {
    position: 'absolute',
    bottom: 30,
    right: 50,
  },
});


export default function UserTableList({ loaded, dataUsers }) {
  const classes = useStyles();


  return (
    <Box pr={2} pl={2} mb={4}>
        { dataUsers.length !== 0 ? 
        <>
        <TableContainer component={Paper}>
          <Table className={classes.table} aria-label="simple table">
              <TableHead>
              <TableRow >
                  <TableCell style={{fontWeight: 700}}>NO</TableCell>
                  <TableCell style={{fontWeight: 700}}>DATE</TableCell>
                  <TableCell style={{fontWeight: 700}}>NAME</TableCell>
                  <TableCell style={{fontWeight: 700}}>EMAIL</TableCell>
                  <TableCell style={{fontWeight: 700}}>ROLE</TableCell>
                  <TableCell style={{fontWeight: 700}}>ACTION</TableCell>
              </TableRow>
              </TableHead>
              <TableBody >
              { loaded && dataUsers.map((row, i) => (
                <TableRow key={i}>
                  <TableCell>{i + 1}</TableCell>
                  <TableCell component="th" scope="row">
                      {moment(row.date).format('l')}
                  </TableCell>
                  <TableCell component="th" scope="row">
                    {row.name}
                  </TableCell>
                  <TableCell component="th" scope="row">
                    {row.email}
                  </TableCell>
                  <TableCell component="th" scope="row">
                    {row.role}
                  </TableCell>
                  
                  <TableCell>
                      <Grid container>
                        <Grid item>
                          <Box pr={1}>
                            <Button variant="contained" color="inherit" size="small" >
                                Detail
                            </Button>
                          </Box>
                        </Grid>
                      </Grid> 
                  </TableCell>
                  
                </TableRow>

              ))}
              </TableBody>
          </Table>
        </TableContainer>
        </>
        : 
        <Paper>
          <Box p={2}>
            You dont have any User yet! 
          </Box>
        </Paper>
        }
    </Box>
  );
}