import React, { useEffect , useState} from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Typography, Box, Button, Link, Dialog, DialogContent, Tooltip  } from '@material-ui/core';
import moment from 'moment';
import { useHistory } from 'react-router-dom'

const useStyles = makeStyles({
  table: {
    minWidth: 500,
  },
});


const HtmlTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: '#3f51b5',
    color: 'rgba(0, 0, 0, 0.87)',
    width: 'fit-content',
    fontSize: theme.typography.pxToRem(12),
    border: '2px solid #3f51b5',
  },
}))(Tooltip);

export default function RecentProducts() {
  const classes = useStyles();
  const history = useHistory()

  let recentProducts = localStorage.getItem("recent_product") ? JSON.parse(localStorage.getItem("recent_product")) : []
  let existingPR = localStorage.getItem("purchase_request") ? JSON.parse(localStorage.getItem("purchase_request")) : []

  const [dataOpenPR, setdataOpenPR] = useState([]);
  const [productsPick, setProductsPick] = useState();

  const [open, setOpen] = React.useState(false);

  const handleClickOpen = (value) => {
    setOpen(true);
    setProductsPick(value)
  };

  const handleClose = () => {
    setOpen(false);
  };

  const onClickUpdate = (value) => {
    history.push({
      pathname: `/update-purchase/${value.id}`,
      state: {  
        product: productsPick
      }
    })
  }

  const onClickCreateNew = () => {
    history.push({
      pathname: `/create-purchases`,
      state: {  
        product: productsPick
      }
    })
  }

  useEffect(() => {
    if(existingPR.length !== 0) {
      let data= [...existingPR]
      let filterOpen = data.filter(val => val.status === "Open")
      setdataOpenPR(filterOpen)
    }
    // eslint-disable-next-line
  }, []);

  return (
    <Box pr={2} pl={2} pt={2}>
        <Box pb={2}>
            <Typography component="h1" variant="h5">
                Recent Products  
            </Typography>
        </Box>
        { recentProducts.length !== 0 ? 
        <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="simple table">
            <TableHead>
            <TableRow >
                <TableCell style={{fontWeight: 700}}>DATE</TableCell>
                <TableCell style={{fontWeight: 700}}>VENDOR</TableCell>
                <TableCell style={{fontWeight: 700}}>PRODUCT NAME</TableCell>
                <TableCell style={{fontWeight: 700, width: 150}}>CURRENT PRICE</TableCell>
                <TableCell style={{fontWeight: 700, width: 150}}>PRICE</TableCell>
                <TableCell style={{fontWeight: 700, width: 50}}>TOTAL LAST 30D</TableCell>
                <TableCell style={{fontWeight: 700, width: 150}}>ACTION</TableCell>
            </TableRow>
            </TableHead>
            <TableBody>
            {recentProducts && recentProducts.map((row, i) => (
                <TableRow key={i}>
                <TableCell component="th" scope="row">
                    {moment().format("l")}
                </TableCell>
                <TableCell component="th" scope="row">
                    {row.vendor_name}
                </TableCell>
                <TableCell><Link href={`/product/${row.sku}`}>{row.name}</Link></TableCell>
                <TableCell>Rp. {row.price.toLocaleString()}</TableCell>
                <TableCell>Rp. {row.price.toLocaleString()}</TableCell>
                <TableCell>24</TableCell>
                <TableCell>
                    <Button 
                      variant="contained" color="primary" size="small" 
                      onClick={() => handleClickOpen(row)}
                      disabled={localStorage.getItem('role') === "approver" || localStorage.getItem('role') === "accounting"  ? true : false}
                    >
                        Add to PR
                    </Button>
                </TableCell>
                </TableRow>
            ))}
            </TableBody>
        </Table>
        </TableContainer>
        : 
        <Paper>
          <Box p={2}>
            You dont have any Orders yet! 
          </Box>
        </Paper>
        }

        <Dialog 
          onClose={handleClose} 
          aria-labelledby="simple-dialog-title"  
          open={open}
          size="lg"
        >
            <DialogContent>
              <Box pt={2}>
              <div onClick={() => onClickCreateNew()}>
              <Button variant="contained" color="secondary" size="medium">
                  Create new purchase request
              </Button>
              </div>
              </Box>
              <Box pt={2} pb={2} style={{display: 'flex', justifyContent: 'center'}}>OR</Box>
              { dataOpenPR.length === 0 ? 
              <Box pb={4} style={{display: 'flex', justifyContent: 'center'}}>There is no Purchase Request Open</Box> :
              <ul className="list-pr">
                { dataOpenPR && dataOpenPR.map((val, i) => (
                <li key={i}>
                   <HtmlTooltip
                      title={
                        <React.Fragment>
                          <div style={{padding: 10}}>
                            <ul style={{ listStyle: 'none', paddingTop: 10, paddingRight: 5, paddingLeft: 5, color: '#fff' }}>
                              { val.data.map((name, i) => (
                                <li key={i}>{i + 1}. {" "} {name.name}</li>
                              )) }
                            </ul>
                          </div>
                        </React.Fragment>
                      }
                      placement="right"
                      arrow
                    >
                      <div className="buttonhover" onClick={() => onClickUpdate(val)}>
                        Add to PO ID #{val.id}
                      </div>
                  </HtmlTooltip>
                </li>
                )) }
              </ul>}
              
            </DialogContent>
        </Dialog>
    </Box>
  );
}