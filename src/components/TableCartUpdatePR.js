import React, {useEffect, useState}  from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Typography, Box, Button, Grid, CircularProgress } from '@material-ui/core';
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import { find, groupBy, remove } from 'lodash'
import uniqid from 'uniqid';
import swal from 'sweetalert';

const useStyles = makeStyles({
  table: {
    minWidth: 500,
  },
});


export default function TableCartUpdatePR({ data, setDataCart, dataCartAll, view }) {
  const classes = useStyles();

  const [groupCart, setGroupCart] = useState([]);
  const [loaded, setLoaded] = useState(false);
  const [loadingSave, setLoadingSave] = useState(false);
  const [loadingSubmit, setLoadingSubmit] = useState(false);
  const [submit, setSubmit] = useState(false);

  // console.log(groupCart, 'state cart child')

  const onRemoveVendor = (vendor) => {
    let copy = [...groupCart]
    if(copy.find(val => val.vendor_name === vendor)) {
      const removeData = remove(copy, function(n) {
        return n.vendor_name !== vendor
      });
      let items = []
      for(let k = 0; k < removeData.length; k ++ ) {
        for(let h = 0; h < removeData[k].item.length; h++) {
          items.push(removeData[k].item[h])
        }
      }
      setGroupCart(removeData)
      setDataCart(items)
    }
  }

  const onRemoveItem = (sku, vendor) => {
    let copy = [...groupCart]
    const currentVendor = find(copy, ['vendor_name', vendor]);

    if(currentVendor.item.find(val => val.sku === sku)) {
      const unremove_item = remove(currentVendor.item, function(n) {
        return n.sku !== sku
      });
      if(copy.find(val => val.vendor_name === vendor)) {
        copy.find(val =>  val.vendor_name === vendor).item = unremove_item;
        let items = []
        for(let k = 0; k < copy.length; k ++ ) {
          for(let h = 0; h < copy[k].item.length; h++) {
            items.push(copy[k].item[h])
          }
        }
        setGroupCart(copy)
        setDataCart(items)
      }
    }
  }

  const addQuantity = (qty, price, sku) => {
    let copy = [...data]

    if(copy.find(val => val.sku === sku)) {
      let quantity = qty + 1
      let subtotal = price * quantity 

      copy.find(val =>  val.sku === sku).quantity = quantity;
      copy.find(val =>  val.sku === sku).sub_price = subtotal;
      setDataCart(copy)

    }
  }

  const removeQuantity = (qty, price, sku) => {
    let copy = [...data]

    if(qty > 1) {
      if(copy.find(val => val.sku === sku)) {
        let quantity = qty - 1
        let subtotal = price * quantity 
        copy.find(val =>  val.sku === sku).quantity = quantity;
        copy.find(val =>  val.sku === sku).sub_price = subtotal;

        setDataCart(copy)
  
      }
    } 

  }

  function sumSubPrice (data) {
    const totalPriceVendor = data.reduce((total, data) => {
      return total + data.sub_price
    }, 0)
    return totalPriceVendor;
  }

  function sumTotalPrice (data) {
    const totalPriceALl = data.reduce((total, data) => {
      return total + data.sub_price
    }, 0)
    return totalPriceALl;
  }

  function savePurchaseRequest (data) {
    setLoadingSave(true)
    setTimeout(() => {
      let existingPO = localStorage.getItem("purchase_request") ? JSON.parse(localStorage.getItem("purchase_request")) : []
      let temporaryPO = localStorage.getItem("update_pr") ? JSON.parse(localStorage.getItem("update_pr")) : null
      let array = [...existingPO]

      if(temporaryPO !== null) {
        const unremove_item = remove(array, function(n) {
          return n.id !== temporaryPO.id
        });

        let newArray = [...unremove_item]
        let finalPO = {
          id: temporaryPO.id,
          data : data,
          total_price : sumTotalPrice(data),
          author : temporaryPO.author,
          status: 'Open',
          date : new Date()
        }
        newArray.push(finalPO)
        localStorage.setItem('update_pr', JSON.stringify(finalPO))
        localStorage.setItem('purchase_request', JSON.stringify(newArray))
      } else {
        let finalPO = {
            id: temporaryPO !== null ? temporaryPO.id : uniqid(),
            data : data,
            total_price : sumTotalPrice(data),
            author : 'Demo Name',
            status: 'Open',
            date : new Date()
        }
        array.push(finalPO)
        localStorage.setItem('update_pr', JSON.stringify(finalPO))
        localStorage.setItem('purchase_request', JSON.stringify(array))
      }
  
      setLoadingSave(false)
      setSubmit(true)
      swal("Success", "Success saving Purchase Request", 'success' )
    }, 2000);

  }

  function submitPurchaseRequest (data) {
    setLoadingSubmit(true)
    setTimeout(() => {
      let existingPO = localStorage.getItem("purchase_request") ? JSON.parse(localStorage.getItem("purchase_request")) : []
      let temporaryPO = localStorage.getItem("update_pr") ? JSON.parse(localStorage.getItem("update_pr")) : {}
  
      let array = [...existingPO]
      const unremove_item = remove(array, function(n) {
        return n.id !== temporaryPO.id
      });
  
      let newArray = [...unremove_item]
      let finalPO = {
          id: temporaryPO.id,
          data : data,
          total_price : sumTotalPrice(data),
          author : temporaryPO.author,
          status: 'Submitted',
          date : new Date()
      }
      newArray.push(finalPO)
      localStorage.setItem('purchase_request', JSON.stringify(newArray))
      localStorage.removeItem('update_pr')
      setGroupCart([])
      setDataCart([])
      setLoadingSubmit(false)
      swal("Success", "Success Submit Purchase Request", 'success' )
    }, 2000);

  }


  useEffect(() => {
    const proceedCart = () => {
      let copy = [...data]
      
      const convert_array = Object.entries(groupBy(copy, 'vendor_name'));

      var final_data = convert_array.map(function(key) { 
        return { 
          vendor_name: key[0], 
          item: key[1] 
        }; 
      });

      setGroupCart(final_data)
      setLoaded(true)
    }
    proceedCart()
    setSubmit(false)
  }, [data]);


  return (
  <>
    { loaded && groupCart.map((data, key) => (
      <Box pt={2} mb={4} key={key}>
        <Box>
          <Paper>
            <Box p={2} style={{display: 'flex', justifyContent: 'space-between'}}>
              <Typography variant="h6" style={{fontWeight: 700}}>{data.vendor_name}</Typography>
              { view ? null : 
              <Button variant="contained" color="secondary" size="small" onClick={() => onRemoveVendor(data.vendor_name)}>
                Remove All
              </Button>
              }
            </Box>
          </Paper>
        </Box>
        <TableContainer component={Paper}>
          <Table className={classes.table} aria-label="simple table">
              <TableHead>
              <TableRow >
                  <TableCell style={{fontWeight: 700}}>NO</TableCell>
                  <TableCell style={{fontWeight: 700}}>SKU</TableCell>
                  <TableCell style={{fontWeight: 700}}>NAME</TableCell>
                  <TableCell style={{fontWeight: 700}}>VENDOR</TableCell>
                  <TableCell style={{fontWeight: 700}}>PRICE</TableCell>
                  <TableCell style={{fontWeight: 700}}>QUANTITY</TableCell>
                  <TableCell style={{fontWeight: 700}}>SUB TOTAL</TableCell>
                  { view ? null :  <TableCell style={{fontWeight: 700}}>ACTION</TableCell> }
              </TableRow>
              </TableHead>
              <TableBody>
              {data.item.map((value, i) => (
                  <TableRow key={i}>
                  <TableCell component="th" scope="row">
                      {i + 1}
                  </TableCell>
                  <TableCell component="th" scope="row">
                      {value.sku}
                  </TableCell>
                  <TableCell component="th" scope="row">
                      {value.name}
                  </TableCell>
                  <TableCell component="th" scope="row">
                      {value.vendor_name}
                  </TableCell>
                  <TableCell>RP. {parseInt(value.price).toLocaleString()}</TableCell>
                  { view ? 
                   <TableCell component="th" scope="row">
                   {value.quantity}
                  </TableCell> : 
                  <TableCell> 
                    <Grid container>
                        <Grid item>
                          <Box 
                            pr={1} 
                            style={{cursor: 'pointer'}}
                            onClick={() => removeQuantity(value.quantity, value.price, value.sku)}
                          >
                            <RemoveCircleOutlineIcon color="secondary"/>
                          </Box>
                        </Grid>
                        <Grid item>
                          <Box pr={1}>
                            {value.quantity} 
                          </Box>
                        </Grid>
                        <Grid item>
                          <Box 
                            pr={1} 
                            style={{cursor: 'pointer'}}
                            onClick={() => addQuantity(value.quantity, value.price, value.sku)}
                          >
                            <AddCircleIcon color="inherit"/>
                          </Box>
                        </Grid>
                      </Grid>
                  </TableCell> }
                  <TableCell>RP. {parseInt(value.sub_price).toLocaleString()}</TableCell>
                  { view ? null :
                  <TableCell>
                      <Grid container>
                        <Grid item>
                          <Box pr={1} style={{cursor: 'pointer'}} onClick={() => onRemoveItem(value.sku, data.vendor_name )}>
                            <DeleteForeverIcon color="secondary"/>
                          </Box>
                        </Grid>
                      </Grid>
                  </TableCell> }
                  </TableRow>
              ))}
              </TableBody>
          </Table>
        </TableContainer>
        <Box>
          <Paper>
            <Box p={2}>
              <Box style={{display: 'flex', justifyContent: 'space-between'}}>
                <Typography variant="body1" style={{fontWeight: 500}}>Sub Total</Typography>
                <Typography variant="body1" style={{fontWeight: 500}}>Rp. {sumSubPrice(data.item).toLocaleString()}</Typography>
              </Box>
              <Box style={{display: 'flex', justifyContent: 'space-between'}}>
                <Typography variant="body1" style={{fontWeight: 500}}>Discount</Typography>
                <Typography variant="body1" style={{fontWeight: 500, color: 'red'}}>Rp. 0,</Typography>
              </Box>
              <Box style={{display: 'flex', justifyContent: 'space-between'}}>
                <Typography variant="body1" style={{fontWeight: 700}}>Total Price / Vendor</Typography>
                <Typography variant="body1" style={{fontWeight: 700}}>Rp. {sumSubPrice(data.item).toLocaleString()}</Typography>
              </Box>
            </Box>
          </Paper>
        </Box>
    </Box>
    ))}


    <Box pt={2}>
      <Paper >
        <Box p={2}>
          <Box style={{display: 'flex', justifyContent: 'space-between'}}>
            <Typography variant="body1" style={{fontWeight: 500}}>Discount</Typography>
            <Typography variant="body1" style={{fontWeight: 500, color: 'red'}}>Rp. 0,</Typography>
          </Box>
          <Box style={{display: 'flex', justifyContent: 'space-between'}}>
            <Typography variant="h6" style={{fontWeight: 700}}>Total Price All Vendor</Typography>
            <Typography variant="h6" style={{fontWeight: 700}}>Rp. {sumTotalPrice(data).toLocaleString()}</Typography>
          </Box>
        </Box>
      </Paper>
    </Box>
    { view ? null : 
    <Box pt={4} pb={4}>
        <Box style={{display: 'flex', justifyContent: 'flex-end'}}>
          <Button variant="contained" color="default" size="large"  onClick={() => savePurchaseRequest(data)}>
              SAVE PURCHASE REQUEST {loadingSave ? <CircularProgress size={20} color="inherit" style={{marginLeft: 20}} /> : null}
          </Button>
          <Box pl={2}><Button variant="contained" color="primary" size="large" disabled={ submit ? false : true } onClick={() => submitPurchaseRequest(data)}>
              SUBMIT PURCHASE REQUEST  {loadingSubmit ? <CircularProgress size={20} color="inherit" style={{marginLeft: 20}} /> : null}
          </Button></Box>
        </Box>
        
    </Box>
    }

  </>
  );
}