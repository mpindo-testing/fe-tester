import React, { useEffect, useState } from 'react'
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Typography, Box, Button, Grid, Link,
  Dialog, DialogContent, Tooltip } from '@material-ui/core';
import { groupBy } from 'lodash'
import moment from 'moment'
import { useHistory } from 'react-router-dom'

const useStyles = makeStyles({
  table: {
    minWidth: 500,
  },
});

const HtmlTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: '#3f51b5',
    color: 'rgba(0, 0, 0, 0.87)',
    width: 'fit-content',
    fontSize: theme.typography.pxToRem(12),
    border: '2px solid #3f51b5',
  },
}))(Tooltip);

export default function TablePuchaseRequest({ loaded, dataPuchaseReq }) {
  const classes = useStyles();
  const history = useHistory()

  let existingTemplate = localStorage.getItem("template_data") ? JSON.parse(localStorage.getItem("template_data")) : []

  const [dataOpenPR, setdataOpenPR] = useState([]);

  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const onClickPRFromTemplate = (value) => {
    history.push({
      pathname: `/create-purchases`,
      state: {  
        product: value.data
      }
    })
  }


  useEffect(() => {
    if(existingTemplate.length !== 0) {
      let data= [...existingTemplate]
      // let filterOpen = data.filter(val => val.status === "Open")
      setdataOpenPR(data)
    }
    // eslint-disable-next-line
  }, []);




  function checkValidate (data) {
    let vendor = false
    const convert_array = Object.entries(groupBy(data, 'vendor_name'));
    if(convert_array.length > 1) {
      vendor = true
    }

    return vendor;

  }


  return (
    <Box pr={2} pl={2} pt={2} mb={4}>
        <Box pb={4}>
            <Grid container justifyContent="space-between">
              <Grid item>
                <Typography component="h1" variant="h5">
                    List Purchase Request 
                </Typography>
              </Grid>
              { localStorage.getItem('role') === "requester" || localStorage.getItem('role') === "admin" ||  localStorage.getItem('role') === "owner"  ?
              <Grid item>
                <Box style={{ display: 'flex' }}>
                  <Box pr={1} onClick={() => handleClickOpen()}>
                    <Button variant="contained" color="default" size="medium">
                        Create from Template
                    </Button>
                  </Box>
                  <Link href="/create-purchases">
                    <Button variant="contained" color="secondary" size="medium">
                        Create New Purchase Request 
                    </Button>
                  </Link>
                </Box>
              </Grid>
              : null }
            </Grid>
        </Box>
        { dataPuchaseReq.length !== 0 ? 
        <TableContainer component={Paper}>
          <Table className={classes.table} aria-label="simple table">
              <TableHead>
              <TableRow >
                  <TableCell style={{fontWeight: 700}}>PR ID</TableCell>
                  <TableCell style={{fontWeight: 700}}>DATE</TableCell>
                  <TableCell style={{fontWeight: 700}}>AUTHOR</TableCell>
                  <TableCell style={{fontWeight: 700}}>VENDOR</TableCell>
                  <TableCell style={{fontWeight: 700}}>TOTAL PRICE</TableCell>
                  <TableCell style={{fontWeight: 700}}>STATUS</TableCell>
                  <TableCell style={{fontWeight: 700}}>ACTION</TableCell>
              </TableRow>
              </TableHead>
              <TableBody>
              {loaded && dataPuchaseReq.map((row, i) => (
                  <TableRow key={i}>
                  <TableCell component="th" scope="row">
                      {row.id}
                  </TableCell>
                  <TableCell component="th" scope="row">
                      {moment(row.date).format('l')}
                  </TableCell>
                  <TableCell component="th" scope="row">
                      {row.author}
                  </TableCell>
                  <TableCell component="th" scope="row">
                    { checkValidate(row.data) ? "Multi Vendor" : row.data[0].vendor_name }
                  </TableCell>
                  <TableCell>Rp. {row.total_price.toLocaleString()}</TableCell>
                  <TableCell>{row.status}</TableCell>
                  <TableCell>
                      { localStorage.getItem('role') === "requester"  ? 
                      <Grid container>
                        <Grid item>
                          { row.status === "Approved" ?
                          <Box pr={1}>
                          <Button variant="contained" color="default" size="small" href={`/view-orders/${row.id}`}>
                              View 
                          </Button>
                          </Box>  : 
                          row.status === "Submitted" ?
                          <Box pr={1}>
                          <Button variant="contained" color="default" size="small" href={`/view-orders/${row.id}`}>
                              View 
                          </Button>
                          </Box> :
                          <Box pr={1}>
                            <Button disabled={ row.status === "Canceled" ? true : false } variant="contained" color="primary" size="small" href={`/update-purchase/${row.id}`}>
                            { row.status !== "Canceled" ? "Update" : "Canceled" }
                            </Button>
                          </Box> }
                        </Grid>
                      </Grid> :
                      localStorage.getItem('role') === "accounting" ?
                      <Grid container>
                        <Grid item>
                          <Box pr={1}>
                            <Button variant="contained" color="default" size="small" href={`/view-orders/${row.id}`}>
                                View Detail
                            </Button>
                          </Box>
                        </Grid>
                      </Grid> :
                      localStorage.getItem('role') === "admin" ||  localStorage.getItem('role') === "owner" ?

                      <Grid container>
                        <Grid item>
                          <Box pr={1}>
                            { row.status === "Submitted" ?
                            <Button variant="contained" color="primary" size="small" href={`/view-purchase/${row.id}`}>
                                View to Approve
                            </Button>
                            : 
                            row.status === "Open" ? 
                            <Box pr={1}>
                              <Button disabled={ row.status === "Rejected" ? true : false } variant="contained" color="primary" size="small" href={`/update-purchase/${row.id}`}>
                              { row.status !== "Rejected" ? "Update" : "Rejected" }
                              </Button>
                            </Box> :
                            <Button variant="contained" color="default" size="small" href={`/view-orders/${row.id}`}>
                                View Detail
                            </Button>
                            }
                          </Box>
                        </Grid>
                      </Grid> : 
                      <Grid container>
                      <Grid item>
                        <Box pr={1}>
                          { row.status === "Submitted" ?
                          <Button variant="contained" color="primary" size="small" href={`/view-purchase/${row.id}`}>
                              View to Approve
                          </Button>
                          : 
                          <Button variant="contained" color="default" size="small" href={`/view-orders/${row.id}`}>
                              View Detail
                          </Button>
                          }
                        </Box>
                      </Grid>
                    </Grid> 
                      }
                  </TableCell>
                  </TableRow>
              ))}
              </TableBody>
          </Table>
        </TableContainer>
        : 
        <Paper>
          <Box p={2}>
            You dont have any Purchase Request Open! 
          </Box>
        </Paper>
        }
        <Dialog 
          onClose={handleClose} 
          aria-labelledby="simple-dialog-title"  
          open={open}
          size="lg"
        >
          <DialogContent style={{ minWidth: 500}}>
            { dataOpenPR.length === 0 ? 
            <Box pb={4} pt={4} style={{display: 'flex', justifyContent: 'center', flexDirection: 'column', alignItems: 'center'}}>
              <Typography>You dont have any template yet.</Typography> <br/>
              <Link href="/create-template">
                <Button variant="contained" color="secondary" size="medium">
                    Create New Template
                </Button>
              </Link>
            </Box> :
            <ul className="list-pr">
              { dataOpenPR && dataOpenPR.map((val, i) => (
              <li key={i}>
                <HtmlTooltip
                    title={
                      <React.Fragment>
                        <div style={{padding: 10}}>
                          <ul style={{ listStyle: 'none', paddingTop: 10, paddingRight: 5, paddingLeft: 5, color: '#fff' }}>
                            { val.data.map((name, i) => (
                              <li key={i}>{i + 1}. {" "} {name.name}</li>
                            )) }
                          </ul>
                        </div>
                      </React.Fragment>
                    }
                    placement="right"
                    arrow
                  >
                    <div className="buttonhover" onClick={() => onClickPRFromTemplate(val)}>
                      {val.name}
                    </div>
                </HtmlTooltip>
              </li>
              )) }
            </ul>}
            
          </DialogContent>
        </Dialog>
    </Box>
  );
}