import React, { useEffect, useState } from 'react'
import { Container, Grid, Box, Button, Link } from '@material-ui/core'
import CardCustom from './Card';
import ButtonSearch from './ButtonSearch';
import SideBar from './SideBar';
import RecentOders from './RecentOrders';
import RecentProducts from './RecentProducts';
import { useSelector } from 'react-redux'
import productsList from '../Hook/listProducts.json'
import TablePuchaseRequest from './TablePuchaseRequest';

 
function Main() {
    const auth = useSelector(state => state.auth)
    const [dataPuchaseReq, setDataPuchaseReq] = useState([]);
    const [dataPuchaseReqAll, setDataPuchaseReqAll] = useState([]);
    const [loadedAll, setLoadedAll] = useState(false);
    const [loaded, setLoaded] = useState(false);
    const [clickReset, onClickReset] = useState(false);
    
    const [dataProducts, setDataProducts] = useState([]);
    const [loadProducts, setLoadedProducts] = useState(false);

    const [dataVendor, setDataVendor] = useState([]);
    const [loadedVendor, setLoadedVendor] = useState(false);

    const [dataCategory, setDataCategory] = useState([]);
    const [loadedCategory, setLoadedCategory] = useState(false);

    const [valueSearch, setValueSearch] = useState("");
    const [valueVendor, setValueVendor] = useState("");
    const [valueFilter, setValueFilter] = useState("");

    const onClickFilter = (value, name) => {
        setValueFilter(name)
        setValueVendor(value)

    }

    useEffect(() => {
        const loadProducts = () => {
            let data = [...productsList]
            if(valueSearch !== "") {
                let newdata = data.filter(key => key.name === valueSearch)
                setDataProducts(newdata)
                setLoadedProducts(true)
            } else if(valueVendor !== "") {
                let datasearch = []
                // let data = []

                if(valueFilter === "Vendor") {
                    for(let h = 0; h < data.length; h ++) {
                        for(let k=0; k < data[h].meta_data.length ; k ++ ) {
                            if(data[h].meta_data[k].key === "woo_dropshipper") {
                                if(data[h].meta_data[k].value === valueVendor) {
                                    datasearch.push(data[h])
                                }
                            }
                        }
                    }
                } else {
                    for(let j = 0; j < data.length; j ++) {
                        for(let d=0; d < data[j].categories.length ; d ++ ) {
                            if(data[j].categories[d].name === valueVendor) {
                                datasearch.push(data[j])
                            }
                        }
                    }
                }
                setDataProducts(datasearch.filter(a => a.sku !== "" ))
                setLoadedProducts(true)
            } else if(clickReset) {
                setDataProducts(data.filter(a => a.sku !== "" ))
                setLoadedProducts(true)
            } else {
                setDataProducts(data.filter(a => a.sku !== "" ))
                setLoadedProducts(true)
            }
        }
        loadProducts()
        // eslint-disable-next-line
    }, [valueSearch, valueVendor, clickReset]);

    useEffect(() => {
        const loadProducts = () => {
            let data = [...productsList]
            setValueSearch("")
            setValueVendor("")
            setValueFilter("")
            setDataProducts(data)
            setLoadedProducts(true)
        }
        loadProducts()
        // eslint-disable-next-line
    }, [clickReset]);

    const findVendor = (data) => {
        let vendor;
        for(let k=0; k < data.length ; k ++ ) {
            if(data[k].key === "woo_dropshipper") {
                vendor = data[k].value
            }
        }
        return vendor
    }

    const findVendorID = (data) => {
        let vendorID;
        for(let k=0; k < data.length ; k ++ ) {
            if(data[k].key === "woo_dropshipper") {
                vendorID = data[k].id.$numberInt
            }
        }
        return vendorID
    }


    useEffect(() => {
        const getData = () => {
            let existingPO = localStorage.getItem("purchase_request") ? JSON.parse(localStorage.getItem("purchase_request")) : [] 

            const arrayFilter = existingPO.filter(val => val.status === "Approved")
            const arrayPR = existingPO.filter(val => val.status !== "Canceled")

            setDataPuchaseReq(arrayFilter)
            setDataPuchaseReqAll(arrayPR)
            setLoadedAll(true)
            setLoaded(true)
        }   
        const loadFirstVendor = () => {
            let data = [...productsList]
            let vendor = []
            for(let i= 0; i < data.length; i++) {
                for(let k=0; k < data[i].meta_data.length ; k ++ ) {
                    if(data[i].meta_data[k].key === "woo_dropshipper") {
                        vendor.push({
                            name : data[i].meta_data[k].value,
                            id : data[i].meta_data[k].id.$numberInt
                        })
                    }
                }
            }

            const unique = [...new Map(vendor.map(item => [item['name'], item])).values()]

            setDataVendor(unique)
            setLoadedVendor(true)
        }
        const loadFirstCategory = () => {
            let data = [...productsList]
            let category = []
            for(let i= 0; i < data.length; i++) {
                category.push(...data[i].categories)
            }
            const unique = [...new Map(category.map(item => [item['name'], item])).values()]
            setDataCategory(unique)
            setLoadedCategory(true)
        }
        getData() 
        loadFirstVendor()
        loadFirstCategory()
        // eslint-disable-next-line
    }, []);
    
    
    return (
    <Container maxWidth="lg">
        <div className="main-app">
            { auth.login ? 
            <Grid container spacing={3}>
                <Grid item xs={6}>
                    <Link href="/manage-purchases">
                    <Box pl={2}>
                    <Button variant="contained" color="secondary" style={{width: '100%', }} >
                        Manage Purchase Request
                    </Button>
                    </Box>
                    </Link>
                </Grid>
                <Grid item xs={6}>
                    <Link href="/manage-orders">
                    <Box pr={2}>
                    <Button variant="contained" color="primary" style={{width: '100%'}} >
                        Manage Purchase Orders
                    </Button>
                    </Box>
                    </Link>
                </Grid>
                <Grid item xs={12}>
                    
                    <TablePuchaseRequest
                        loaded={loadedAll}
                        dataPuchaseReq={dataPuchaseReqAll}
                    />
                    <RecentOders
                        dataPuchaseReq={dataPuchaseReq}
                        loaded={loaded}
                    />
                    <RecentProducts/>
                </Grid>
            </Grid>
            :
            <Grid container spacing={3}>
                <Grid item xl={3} lg={3} md={3} sm={3} xs={12}>
                    <SideBar 
                        dataVendor={dataVendor}
                        loadedVendor={loadedVendor}
                        dataCategory={dataCategory}
                        loadedCategory={loadedCategory}
                        onClickFilter={onClickFilter}
                        onClickReset={onClickReset}
                        clickReset={clickReset}
                    />
                </Grid>
                <Grid item xl={9} lg={9} md={9} sm={9} xs={12}>
                    <Box>
                        <ButtonSearch setValueSearch={setValueSearch} valueSearch={valueSearch}/>
                    </Box>
                    <Box mt={2} >
                        
                        <Box>
                            { loadProducts && dataProducts.length !== 0 ?
                            <Grid container spacing={2}>
                                { loadProducts && dataProducts.map((data, i) => (
                                    <Grid item key={i} lg={4}>
                                        <CardCustom 
                                            name={data.name} 
                                            image={data.images[0].src}
                                            sku={data.sku}
                                            price={data.price}
                                            vendor={findVendor(data.meta_data)}
                                            vendorID={findVendorID(data.meta_data)}

                                        />
                                    </Grid>
                                )) }
                            </Grid>
                            : 
                            <Box>
                                Products not found.
                            </Box>
                            }
                        </Box>
                        
                    
                    </Box>
                </Grid>
            </Grid>
            }
        </div>
    </Container> 
    )
}

export default Main
