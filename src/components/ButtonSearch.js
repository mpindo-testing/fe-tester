import React, { useState, useEffect } from 'react'
import { Paper } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';

const useStyles = makeStyles((theme) => ({
  rootInput: {
    display: 'flex',
    width: '100%',
    marginBottom: 30
  },
  input: {
    marginLeft: theme.spacing(3),
    flex: 1,
    height: 50
  },
  iconButton: {
    padding: 10,
  }
}));

function ButtonSearch({ setValueSearch, valueSearch }) {
    const classes = useStyles();

    const [searchValue, setsearchValue] = useState("");


    const enterSearch = (e) => {
      e.preventDefault()
      setValueSearch(searchValue)
    }

    useEffect(() => {
      setsearchValue(valueSearch)
    }, [valueSearch]);


    return (
        <div>
            <Paper component="form" onSubmit={enterSearch} className={classes.rootInput}>
                <InputBase
                    className={classes.input}
                    placeholder="Search Products"
                    onChange={(e) => setsearchValue(e.target.value)}
                    value={searchValue}
                />
                <IconButton type="submit" className={classes.iconButton} aria-label="search">
                    <SearchIcon />
                </IconButton>
            </Paper>
        </div>
    )
}

export default ButtonSearch
