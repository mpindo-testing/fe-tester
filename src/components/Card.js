import React, { useEffect, useState } from 'react'
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Dialog, DialogContent, Tooltip } from '@material-ui/core';
import { Box, Link } from '@material-ui/core';
import { useHistory } from 'react-router-dom'
import {  useSelector } from 'react-redux'

const HtmlTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: '#3f51b5',
    color: 'rgba(0, 0, 0, 0.87)',
    width: 'fit-content',
    fontSize: theme.typography.pxToRem(12),
    border: '2px solid #3f51b5',
  },
}))(Tooltip);

const useStyles = makeStyles({
  root: {
    width: 290,
    margin: 10
  },
  imagecenter: {
    display: 'flex',
    justifyContent: 'center',
  },
  media: {
    height: 200,
    width: 200,
  },
  button: {
      width: '100%'
  },
  viewprice: {
      cursor: 'pointer'
  }
});

export default function CardCustom({ name, image, sku, price, vendorID, vendor }) {
  const classes = useStyles();
  const history = useHistory()

  const auth = useSelector(state => state.auth)


  let existingPR = localStorage.getItem("purchase_request") ? JSON.parse(localStorage.getItem("purchase_request")) : []
  let existingTemplate = localStorage.getItem("template_data") ? JSON.parse(localStorage.getItem("template_data")) : []

  const [dataOpenPR, setdataOpenPR] = useState([]);
  const [dataOpenTemplate, setDataOpenTemplate] = useState([]);
  const [productsPick, setProductsPick] = useState();
  const [productsPickTemplate, setProductsPickTemplate] = useState();

  const [open, setOpen] = React.useState(false);
  const [openTemplate, setOpenTemplate] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
    let product_detail = {
      sku: sku,
      name: name,
      sub_price : parseInt(price),
      price: parseInt(price),
      quantity: 1,
      vendor_id : vendorID,
      vendor_name : vendor
    }
    setProductsPick(product_detail)
  };

  const handleClickOpenTemplate = () => {
    setOpenTemplate(true);
    let product_detail = {
      sku: sku,
      name: name,
      sub_price : parseInt(price),
      price: parseInt(price),
      quantity: 1,
      vendor_id : vendorID,
      vendor_name : vendor
    }
    setProductsPickTemplate(product_detail)
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleCloseTemplate = () => {
    setOpenTemplate(false);
  };

  const onClickUpdate = (value) => {
    history.push({ 
      pathname: `/update-purchase/${value.id}`,
      state: {  
        product: productsPick
      }
    })
  }

  const onClickUpdateToTemplate = (value) => {
    history.push({ 
      pathname: `/update-template/${value.id}`,
      state: {  
        product: productsPickTemplate
      }
    })
  }

  const onClickCreateNew = () => {
    history.push({
      pathname: `/create-purchases`,
      state: {  
        product: productsPick
      }
    })
  }

  const onClickCreateNewtemplate = () => {
    history.push({
      pathname: `/create-template`,
      state: {  
        product: productsPickTemplate
      }
    })
  }


  useEffect(() => {
    if(existingPR.length !== 0) {
      let data= [...existingPR]
      let filterOpen = data.filter(val => val.status === "Open")
      setdataOpenPR(filterOpen)
    }
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if(existingTemplate.length !== 0) {
      let data= [...existingTemplate]
      setDataOpenTemplate(data)
    }
    // eslint-disable-next-line
  }, []);




  return (
  <>
    <Card className={classes.root}>
      <CardActionArea className={classes.imagecenter}>
        <Link href={`/product/${sku}`}>
        <CardMedia
          className={classes.media}
          image={image}
          title="product"
        />
        </Link>
      </CardActionArea>
        <CardContent style={{ minHeight: 130 }}>
          <Typography gutterBottom variant="body1" style={{fontWeight: 600}}>
            {name}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p" className={classes.viewprice}>
            Login to View Price
          </Typography>
        </CardContent>
      <CardActions>
      {auth.login ?
          <div style={{display: 'flex', justifyContent: 'space-between'}}>
              <Box pr={1} style={{width: 'fit-content'}}>
                <Button 
                  size="small" color="primary" variant="contained" 
                  onClick={() => handleClickOpen()}
                  disabled={localStorage.getItem('role') === "approver" || localStorage.getItem('role') === "accounting"  ? true : false}
                >
                  Add to PR
                </Button>
              </Box>
              <Box style={{width: 'fit-content'}}>
                <Button 
                  size="small" color="default" variant="contained" 
                  onClick={() => handleClickOpenTemplate()}
                  disabled={localStorage.getItem('role') === "approver" || localStorage.getItem('role') === "accounting"  ? true : false}
                >
                  Add to Template
                </Button>
              </Box>
          </div> :
          <div style={{width: '100%'}}>
            <Box pr={1} style={{width: '100%'}}>
              <Button 
                style={{width: '100%'}}
                size="small" color="primary" variant="contained" 
                href="/login"
              >
                Login to BUy
              </Button>
            </Box>
          </div> }
      </CardActions>
    </Card>

  <Dialog 
    onClose={handleClose} 
    aria-labelledby="simple-dialog-title"  
    open={open}
    size="lg"
    >
      <DialogContent>
        <Box pt={2}>
        <div onClick={() => onClickCreateNew()}>
        <Button variant="contained" color="secondary" size="medium">
            Create new purchase request
        </Button>
        </div>
        </Box>
        <Box pt={2} pb={2} style={{display: 'flex', justifyContent: 'center'}}>OR</Box>
        { dataOpenPR.length === 0 ? 
        <Box pb={4} style={{display: 'flex', justifyContent: 'center'}}>There is no Purchase Request Open</Box> :
        <ul className="list-pr">
          { dataOpenPR && dataOpenPR.map((val, i) => (
          <li key={i}>
            <HtmlTooltip
                title={
                  <React.Fragment>
                    <div style={{padding: 10}}>
                      <ul style={{ listStyle: 'none', paddingTop: 10, paddingRight: 5, paddingLeft: 5, color: '#fff' }}>
                        { val.data.map((name, i) => (
                          <li key={i}>{i + 1}. {" "} {name.name}</li>
                        )) }
                      </ul>
                    </div>
                  </React.Fragment>
                }
                placement="right"
                arrow
              >
                <div className="buttonhover" onClick={() => onClickUpdate(val)}>
                  Add to PO ID #{val.id}
                </div>
            </HtmlTooltip>
          </li>
          )) }
        </ul>}
        
      </DialogContent>
    </Dialog>

    <Dialog 
        onClose={handleCloseTemplate} 
        aria-labelledby="simple-dialog-title"  
        open={openTemplate}
        size="lg"
      >
        <DialogContent style={{ minWidth: 500}}>
          { dataOpenTemplate.length === 0 ? 
          <Box pb={4} pt={4} style={{display: 'flex', justifyContent: 'center', flexDirection: 'column', alignItems: 'center'}}>
            <Typography>You dont have any template yet.</Typography> <br/>
            <Box onClick={() => onClickCreateNewtemplate()}>
              <Button variant="contained" color="secondary" size="medium">
                  Create New Template
              </Button>
            </Box>
          </Box> :
          <>
          <Box pt={2} style={{width: '100%'}}>
            <Box onClick={() => onClickCreateNewtemplate()}>
              <Button fullWidth variant="contained" color="secondary" size="medium">
                  Create New Template
              </Button>
            </Box>
          </Box>
          <Box pt={2} style={{display: 'flex', justifyContent: 'center'}}>OR</Box>

          <ul className="list-pr">
            { dataOpenTemplate && dataOpenTemplate.map((val, i) => (
            <li key={i}>
              <HtmlTooltip
                  title={
                    <React.Fragment>
                      <div style={{padding: 10}}>
                        <ul style={{ listStyle: 'none', paddingTop: 10, paddingRight: 5, paddingLeft: 5, color: '#fff' }}>
                          { val.data.map((name, i) => (
                            <li key={i}>{i + 1}. {" "} {name.name}</li>
                          )) }
                        </ul>
                      </div>
                    </React.Fragment>
                  }
                  placement="right"
                  arrow
                >
                  <div className="buttonhover" onClick={() => onClickUpdateToTemplate(val)}>
                    Add to {val.name}
                  </div>
              </HtmlTooltip>
            </li>
            )) }
          </ul> 
          </>}
          
        </DialogContent>
      </Dialog>
</>
  );
}