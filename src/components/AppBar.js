import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { Button, Typography, Box, Container, Popover } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux'
import { logOut } from './../redux/authentication/actionCreator'
import { Link } from 'react-router-dom'
import AccountCircle from '@material-ui/icons/AccountCircle';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
    marginBottom: 10
  },
  menuNav: {
      marginLeft: 50,
      display: 'flex',
      color: 'white'
  },
  rightNav: {
      display: 'flex'
  },
  root: {
    width: '100%',
    minWidth: 160,
    backgroundColor: theme.palette.background.paper,
  },
  
}));

export default function Navbar() {
  const classes = useStyles();
  const dispatch = useDispatch()

  const auth = useSelector(state => state.auth)

  // dropdown menu 
  const [anchorEl, setAnchorEl] = React.useState(null);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const open = Boolean(anchorEl);
  const id = 2 ? 'simple-popover' : undefined;


  function ListItemLink(props) {
    return <ListItem button component="a" {...props} />;
  }

  return (
    <div className={classes.grow}>
      <AppBar position="static">
        <Container>
        <Toolbar style={{marginLeft: 0, marginRight: 0}}>
          <a href="/">
            <Typography variant="h5" style={{color: 'white'}}>
              MPI
            </Typography>
          </a>
          {/* <div className={classes.menuNav}>
            <Box pr={2}>
                <Typography variant="h6">
                    About Us
                </Typography>
            </Box>
            <Box>
                <Typography variant="h6">
                Contact Us
                </Typography>
            </Box>
          </div> */}
          <div className={classes.grow} />

          { auth.login ? 
          <div className={classes.rightNav}>
            <div>
              <Box pr={2}>
              <Link to="/browse">
                <Button color="default" variant='contained' size="small">
                    Browse Product
                </Button>
              </Link>
              </Box>
            </div>
            <div style={{display: 'flex', cursor: 'pointer'}} onClick={handleClick}>
              <Box> <AccountCircle/>  </Box>
              <Box pl={1}> {localStorage.getItem('name')}</Box>
              <Box pl={1}> <ArrowDropDownIcon/></Box>
            </div>

            {/* Dropdown Menu */}
            <Popover
              id={id}
              open={open}
              anchorEl={anchorEl}
              onClose={handleClose}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              className="popper-style"
            >
              <Box>
                <div className={classes.root}>
                  <List component="nav" aria-label="secondary mailbox folders">
                    <ListItemLink href="/manage-orders">
                      <ListItemText primary="Manage PO" />
                    </ListItemLink>
                    <ListItemLink href="/manage-purchases">
                      <ListItemText primary="Manage PR" />
                    </ListItemLink>
                     <ListItemLink  href="/my-template">
                      <ListItemText primary="My Template" />
                    </ListItemLink>
                    <ListItemLink  href="/company-detail">
                      <ListItemText primary="Company Detail" />
                    </ListItemLink>
                    <ListItemLink href="/user-management">
                      <ListItemText primary="User Management" />
                    </ListItemLink>
                    <ListItem button onClick={() => dispatch(logOut())}>
                      <ListItemText primary="Sign Out" />
                    </ListItem>
                   
                  </List>
                </div>
              </Box>
            </Popover>
           
          </div>
          :
          <div className={classes.rightNav}>
            <Box pr={2}>
              <Link to="/login">
                <Button color="secondary" variant='contained'>
                    Login
                </Button>
              </Link>
            </Box>
            <Box>
              <Link to="/register">
                <Button color="inherit" variant='contained'>
                    Register
                </Button>
              </Link>
            </Box>
          </div>
          }
          
        </Toolbar>
        </Container>
      </AppBar>
    </div>
  );
}