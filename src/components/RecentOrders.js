import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Typography, Box, Button, Grid, Tooltip } from '@material-ui/core';
import { groupBy } from 'lodash';
import moment from 'moment';


const useStyles = makeStyles({
  table: {
    minWidth: 500,
  },
  absolute: {
    position: 'absolute',
    bottom: 30,
    right: 50,
  },
});


const HtmlTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: '#7be4d3',
    color: 'rgba(0, 0, 0, 0.87)',
    minWidth: 400,
    height: 'fit-content',
    fontSize: theme.typography.pxToRem(15),
    border: '1px solid #dadde9',
  },
}))(Tooltip);

export default function RecentOrders({ loaded, dataPuchaseReq }) {
  const classes = useStyles();

  function checkValidate (data) {
    let vendor = false
    const convert_array = Object.entries(groupBy(data, 'vendor_name'));
    if(convert_array.length > 1) {
      vendor = true
    } 

    return vendor;

  }

  return (
    <Box pr={2} pl={2} pt={2} mb={4}>
        <Box pb={4}>
            <Grid container justifyContent="space-between">
              <Grid item>
                <Typography component="h1" variant="h5">
                    List Purchase Orders 
                </Typography>
              </Grid>
            </Grid>
        </Box>
        { dataPuchaseReq.length !== 0 ? 
        <>
        <TableContainer component={Paper}>
          <Table className={classes.table} aria-label="simple table">
              <TableHead>
              <TableRow >
                  <TableCell style={{fontWeight: 700}}>PO ID</TableCell>
                  <TableCell style={{fontWeight: 700}}>DATE</TableCell>
                  <TableCell style={{fontWeight: 700}}>VENDOR</TableCell>
                  <TableCell style={{fontWeight: 700}}>TOTAL PRICE</TableCell>
                  <TableCell style={{fontWeight: 700}}>STATUS</TableCell>
              </TableRow>
              </TableHead>
              <TableBody >
              {loaded && dataPuchaseReq.map((row, i) => (
                <TableRow key={i}>
                  <TableCell >
                    <HtmlTooltip
                        title={
                          <React.Fragment>
                            <div style={{paddingTop: 15, paddingRight: 10, paddingLeft: 10, width: 'fit-content'}}>
                              <ul style={{ listStyle: 'none', padding: 0 }}>
                                { row.data.map((name, i) => (
                                  <li key={i}>{i + 1}. {" "} {name.name} ({name.vendor_name})</li>
                                )) }
                              </ul>
                            </div>
                          </React.Fragment>
                        }
                        placement="right"
                        arrow
                      >
                      <div style={{ cursor: 'pointer', color: 'red'}}>
                        {row.id}
                      </div>
                    </HtmlTooltip>
                  </TableCell>
                  <TableCell component="th" scope="row">
                      {moment(row.date).format('l')}
                  </TableCell>
                  <TableCell component="th" scope="row">
                  { checkValidate(row.data) ? "Multi Vendor" : row.data[0].vendor_name }
                  </TableCell>
                  <TableCell>Rp. {row.total_price.toLocaleString()}</TableCell>
                  <TableCell>{row.status}</TableCell>
                  <TableCell>
                      <Grid container>
                        <Grid item>
                          <Box pr={1}>
                            <Button variant="contained" color="inherit" size="small" href={`/view-orders/${row.id}`}>
                                Detail
                            </Button>
                          </Box>
                        </Grid>
                      </Grid> 
                  </TableCell>
                  
                </TableRow>

              ))}
              </TableBody>
          </Table>
        </TableContainer>
        </>
        : 
        <Paper>
          <Box p={2}>
            You dont have any Purchase Orders yet! 
          </Box>
        </Paper>
        }
    </Box>
  );
}